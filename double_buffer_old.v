// Double buffered frame mem with read mask
// if choose == 0, buf1 is written to, buf0 is read
// if choose == 1, buf0 is written to, buf1 is read
// when choose is flipped, read mask is reset to simulate memory clearing
module double_buffer(input clk,
                     input [7:0] d, output [7:0] q,
                     input [8:0] write_addr, input [8:0] read_addr,
                     input choose, input write_en);

  wire [7:0] q0, q1;
  reg [511:0] read_mask0 = 0, read_mask1 = 0;
  reg old_choose = 0;
  wire reset_mask;

  initial begin
    read_mask0 = 0;
    read_mask1 = 0;
  end

  frame_mem buf0(clk, q0, d, write_addr, read_addr, write_en & choose);
  frame_mem buf1(clk, q1, d, write_addr, read_addr, write_en & !choose);

  always@(posedge clk) old_choose <= choose;
  assign reset_mask0 = choose & !old_choose;
  assign reset_mask1 = !choose & old_choose;

   always@(posedge clk or posedge reset_mask0) begin
    if (reset_mask0) begin
      read_mask0 <= 0;
    end
    else
      if (write_en) begin
        read_mask0 <= read_mask0 | (1 << write_addr);
      end
  end

   always@(posedge clk or posedge reset_mask1) begin
    if (reset_mask1) begin
      read_mask1 <= 0;
    end
    else
      if (write_en) begin
        read_mask1 <= read_mask1 | (1 << write_addr);
      end
  end
 
  assign q = choose ? (read_mask1[read_addr] ? q1 : 8'h0) : (read_mask0[read_addr] ? q0 : 8'h0);

endmodule
