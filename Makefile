TESTBENCHES=double_buffer_tb rasterizer_tb

double_buffer_tb: double_buffer_tb.v double_buffer.v frame_mem.v
	iverilog $^ -o $@

rasterizer_tb: rasterizer_tb.v rasterizer.v double_buffer.v frame_mem.v
	iverilog $^ -o $@

testbenches: ${TESTBENCHES}

run_testbenches: ${TESTBENCHES}
	for tb in $^; do vvp -n $$tb -lxt2; done

test: testbenches run_testbenches

clean:
	rm ${TESTBENCHES} *lxt
