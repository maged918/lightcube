//`define SIMULATE 1

module sdcard_reader (input clk,
                      input spi_miso, output spi_n_cs, output spi_clk, output spi_mosi,
                      input [31:0] frame_num, input reset, output [33:0] debug,
                      output [4:0] rastr_func, output reg [8:0] rastr_pos, output reg [7:0] rastr_val);

// SIMULATION
`ifdef SIMULATE
  parameter SPI_INIT_FREQ = 25_000_000;
  parameter SPI_OP_FREQ = 25_000_000;
  parameter INIT_DELAY = 16;
  parameter RESPONSE_TIMEOUT = 8;
  parameter BLOCK_TIMEOUT = 8;
  parameter BLOCK_SIZE = 8;
`else
  parameter SPI_INIT_FREQ = 400_000;
  parameter SPI_OP_FREQ = 25_000_000;
  parameter INIT_DELAY = 8 * 128;
  parameter RESPONSE_TIMEOUT = 512;
  parameter BLOCK_TIMEOUT = 2048;
  parameter BLOCK_SIZE = 512;
`endif

  parameter FRAME_SIZE = 512;

// DEBUG
  reg [3:0] debug_n_timeouts, debug_n_replies;
  reg [7:0] debug_last_cmd;
  reg [15:0] debug_replies;
  assign debug = {card_ready, debug_n_timeouts, debug_n_replies, debug_last_cmd, debug_replies};

// SPI Control and initialization delay
  reg [31:0] spi_freq = SPI_INIT_FREQ;
  wire spi_next_byte;
  reg spi_start = 0;
  wire [7:0] spi_byte_incoming;
  reg [7:0] spi_byte_outgoing = 8'hFF;

  spi_master m(clk, spi_freq, spi_start, spi_next_byte,
               spi_byte_incoming, spi_byte_outgoing,
               spi_clk, spi_mosi, spi_miso, spi_n_cs);

  reg [$clog2(INIT_DELAY):0] init_delay = 0;

  always@(negedge spi_clk or negedge reset) begin
    if (!reset) begin
      init_delay <= 0;
      spi_start <= 0;
      spi_freq <= SPI_INIT_FREQ;
    end
    else begin
      if (init_delay <= INIT_DELAY) init_delay <= init_delay + 1;
      spi_freq <= card_ready == 2 ? SPI_OP_FREQ : SPI_INIT_FREQ;
      spi_start <= init_delay > INIT_DELAY;
    end
  end

// Rasterizer Control
  assign rastr_func = 1;

// Frame number synchronization
  reg [31:0] cur_frame_num, block_addr;
  always@(posedge clk) begin
    block_addr <= frame_num * FRAME_SIZE;
  end

// State Machine
  // FIXME rename to STATE_*
  parameter CMD_SEND_BYTE_1 = 1;
  parameter CMD_SEND_BYTE_2 = 2;
  parameter CMD_SEND_BYTE_3 = 3;
  parameter CMD_SEND_BYTE_4 = 4;
  parameter CMD_SEND_BYTE_5 = 5;
  parameter CMD_SEND_BYTE_6 = 6;

  parameter CMD_WAIT_RESP    = 7;
  parameter CMD_NO_WAIT_RESP = 7;
  parameter CMD_WAIT_RESP_R1 = 8;

  parameter CMD_WAIT_BLOCK_START = 9;
  parameter CMD_READ_BLOCK = 10;

  parameter CMD_WAIT_FOR_FRAME_NUM = 11;


  reg [9:0] cmd_comm_state;
  reg [31:0] resp_timeout;
  reg [1:0] card_ready;
  reg [47:0] cmd_buffer;
  reg [9:0] block_cnt;

  always@(posedge spi_next_byte or negedge reset) begin
    if (!reset) begin
      spi_byte_outgoing <= 8'hFF;
      cmd_buffer <= {2'b01, 6'd0, 40'h95}; // start with CMD0
      cmd_comm_state <= CMD_SEND_BYTE_1;
      resp_timeout <= 0;
      card_ready <= 0;
      cur_frame_num <= 0;
      block_cnt <= 0;

      debug_n_timeouts <= 0;
      debug_n_replies <= 0;
      debug_last_cmd <= 0;
      debug_replies <= 0;
    end
    else begin
      (* parallel_case *) case (cmd_comm_state)
      CMD_SEND_BYTE_1: begin
        debug_last_cmd <= {2'b00, cmd_buffer[45:40]};
        resp_timeout <= RESPONSE_TIMEOUT;
        cmd_comm_state <= cmd_comm_state + 1;
        spi_byte_outgoing <= cmd_buffer[47:40];
      end
      CMD_SEND_BYTE_2: begin
        spi_byte_outgoing <= cmd_buffer[39:32];
        cmd_comm_state <= cmd_comm_state + 1;
      end
      CMD_SEND_BYTE_3: begin
        spi_byte_outgoing <= cmd_buffer[31:24];
        cmd_comm_state <= cmd_comm_state + 1;
      end
      CMD_SEND_BYTE_4: begin
        spi_byte_outgoing <= cmd_buffer[23:16];
        cmd_comm_state <= cmd_comm_state + 1;
      end
      CMD_SEND_BYTE_5: begin
        spi_byte_outgoing <= cmd_buffer[15:8];
        cmd_comm_state <= cmd_comm_state + 1;
      end
      CMD_SEND_BYTE_6: begin
        spi_byte_outgoing <= cmd_buffer[7:0];
        cmd_comm_state <= CMD_WAIT_RESP_R1;
      end
      CMD_NO_WAIT_RESP: begin //FIXME UNUSED
        spi_byte_outgoing <= 8'hFF;
        cmd_comm_state <= CMD_SEND_BYTE_1;
      end

      CMD_WAIT_RESP_R1: begin
        spi_byte_outgoing <= 8'hFF;
        if (resp_timeout != 0) begin
          if (spi_byte_incoming[7] == 0) begin // responses start with a 0
            debug_n_replies = debug_n_replies + 1;
            debug_replies <= {debug_replies[7:0], spi_byte_incoming};
            (* parallel_case *)  case (card_ready)
            0: begin // response for CMD0
              card_ready <= 1;
              cmd_buffer <= {2'b01, 6'd1, 40'h95}; // CMD1: ready?
              cmd_comm_state <= CMD_SEND_BYTE_1;
            end
            1: begin // response for CMD1
              if (spi_byte_incoming[0] == 0) begin // in idle state
                card_ready <= 2;
                cmd_buffer <= {2'b01, 6'd17, block_addr, 8'h95}; // CMD17: block read
              end
              // if not idle, CMD1 is resent
              cmd_comm_state <= CMD_SEND_BYTE_1;
            end
            2: begin // response for CMD17
              // FIXME check for errors?
              resp_timeout <= BLOCK_TIMEOUT;
              cmd_comm_state <= CMD_WAIT_BLOCK_START;
            end
            endcase
          end
          else // no response
            resp_timeout <= resp_timeout - 1;
        end
        else begin // timed out, resend command
          debug_n_timeouts <= debug_n_timeouts + 1;
          cmd_comm_state <= CMD_SEND_BYTE_1;
        end
      end

      CMD_WAIT_BLOCK_START: begin
        spi_byte_outgoing <= 8'hFF;
        if (resp_timeout != 0) begin
          // check for block start token
          if (spi_byte_incoming == 8'b1111_1110) begin
            cmd_comm_state <= CMD_READ_BLOCK;
            block_cnt <= 0;
          end
          else
            resp_timeout <= resp_timeout - 1;
        end
        else begin // timed out while waiting for a block, reset
          // FIXME proper way is to deassert CS and reinit, not just send CMD0
          debug_n_timeouts <= debug_n_timeouts + 1;
          card_ready <= 1;
          cmd_buffer <= {2'b01, 6'd1, 40'h95}; // CMD1
          cmd_comm_state <= CMD_SEND_BYTE_1;
        end
      end

      // NOTE: the 2 CRC bytes are ignored implicitly by the time it takes to
      // change state CMD_READ_BLOCK -> CMD_WAIT_FOR_FRAME_NUM
      CMD_READ_BLOCK: begin
        spi_byte_outgoing <= 8'hFF;
        if (block_cnt < BLOCK_SIZE) begin
          debug_replies <= {debug_replies[7:0], spi_byte_incoming};
          block_cnt <= block_cnt + 1;
          rastr_pos <= {block_cnt[2:0], block_cnt[5:3], block_cnt[8:6]};
          rastr_val <= spi_byte_incoming;
        end
        else // current byte is 1st CRC byte, block is done
          cmd_comm_state <= CMD_WAIT_FOR_FRAME_NUM;
      end

      CMD_WAIT_FOR_FRAME_NUM: begin // wait for frame_num to change
         spi_byte_outgoing <= 8'hFF;
         cur_frame_num <= frame_num;
         // start a new read cycle if frame_num has changed
         if (frame_num != cur_frame_num) begin
           cmd_buffer <= {2'b01, 6'd17, block_addr, 8'h95}; // CMD17: block read
           cmd_comm_state <= CMD_SEND_BYTE_1;
         end
      end

      endcase
    end
  end

endmodule
