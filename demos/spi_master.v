module spi_master(input clk_50, input [31:0] spi_freq, input start,
                  output reg next_byte,
                  output reg [7:0] byte_incoming, input [7:0] byte_outgoing,
                  output reg sck, output mosi, input miso, output n_cs);

  initial begin
    sck = 0;
  end

  reg [7:0] byte_ring = 8'hFF;
  reg [2:0] bit_count = 3'h0;
  reg miso_last = 0;
  reg started = 0;

  // Divide the 50MHz clock to generate a suitable SPI clk
  reg [31:0] sck_clk_div = 0;
  always@(posedge clk_50) begin
    if (sck_clk_div >= 50_000_000 / spi_freq - 1) begin
      sck_clk_div <= 0;
      sck <= !sck;
    end
    else sck_clk_div <= sck_clk_div + 1;

   // next_byte is triggered every byte received/sent
   next_byte <= started && bit_count == 0;
  end

  // Chip Select is active low
  assign n_cs = !start;

  // in the first cycle, byte_ring hasn't been loaded yet
  assign mosi = (bit_count == 0) ? byte_outgoing[7] : byte_ring[7];

  always@(posedge sck) miso_last <= miso;

  always@(negedge sck or negedge start) begin
    if (!start) begin
      byte_ring <= 8'hFF;
      bit_count <= 3'h0;
      byte_incoming <= 8'hFF;
      byte_ring <= byte_outgoing;
      started <= 0;
    end
    else begin
      started <= 1;
      bit_count <= bit_count + 1;
      case (bit_count)
        // in the first cycle, load byte_ring
        0: byte_ring <= {byte_outgoing[6:0], miso_last};
        // in the last cycle, update byte_incoming; next_byte was pulsed and
        // byte_outgoing will be ready in the next cycle
        7: byte_incoming <= {byte_ring[6:0], miso_last};
        // All other cycles, shift byte_ring
        default: byte_ring <= {byte_ring[6:0], miso_last};
      endcase
    end
  end

endmodule
