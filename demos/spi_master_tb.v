`timescale 1ns/1ns
module spi_master_tb();

  reg clk_50;
  reg start;
  reg [31:0] spi_freq;
  reg [7:0] byte_outgoing;
  wire next_byte;
  reg [7:0] received;
  wire [7:0] byte_incoming;

  initial begin
    $dumpfile("spi_master_tb.lxt");
    $dumpvars(0, spi_master_tb);
    clk_50 = 0;
    start = 0;
    spi_freq = 25000000;
    byte_outgoing <= 8'b0111_1111;
    #100 start = 1;
    #99999 start = 0;
    #20 $stop();
  end

  always #10 clk_50 = !clk_50;

  spi_master spi(.clk_50(clk_50), .spi_freq(spi_freq), .start(start),
             .byte_outgoing(byte_outgoing), .byte_incoming(byte_incoming),
             .next_byte(next_byte), .miso(1));

  always@(posedge next_byte) begin
    byte_outgoing <= byte_outgoing + 1;
    received <= byte_incoming;
  end

endmodule
