import transformations
import numpy as np

FRAMERATE = 25

# NOTES
# Points are np.array()s of floats
# Polygons are python lists or np.arrays() of points

# TODO
# Pass color/level data to draw functions?

class FramePlotter(object):
  # number of virtual points between each cube voxel
  SUBSAMPLES = 5
  # width of anti-aliased line
  AA_WIDTH = 1.

  BOX_POINTS = np.array([[0, 0, 0], [0, 0, 1], [0, 1, 0],
                         [0, 1, 1], [1, 0, 0], [1, 0, 1],
                         [1, 1, 0], [1, 1, 1]])

  def clear_frame(self):
    self.frame = np.zeros(shape = self.size, dtype = np.uint8)

  def clamp_point(self, pt):
    d = pt.dtype
    pt = map(lambda m, x: min(m, max(x, 0)), self.size-1, pt)
    return np.array(pt, dtype=d)

  def __init__(self, f, length = 8, width = 8, height = 8):
    self.size = np.array([length, width, height])
    self.clear_frame()
    if isinstance(f, str):
      self.out_file = open(f, 'wb')
    elif hasattr(f, 'write'):
      self.out_file = f
    else:
      raise Exception("Invalid output argument")

  #FIXME polygon and shape fill
  #FIXME OPTIMZE don't use the subsampling method, find another way to compute the set
  #        of intersecting boxes
  #FIXME OPTIMIZE collect all polygons to be drawn, then draw during
  #        output_frame()
  def draw(self, polys):
    for poly in polys:
      pt1 = poly[-1][:3]
      for pt2 in poly: # render line pt2-pt1
        pt2 = pt2[:3]
        v = (pt2 - pt1)
        l = np.linalg.norm(v)
        u = v/l
        last_box = None
        # find all the boxes that the line passes through
        for x in np.linspace(0, l, l * self.SUBSAMPLES):
          pt = pt1 + (u*x)
          oob = False
          cur_box = np.uint32(np.floor(pt))
          for i in xrange(3):
            if cur_box[i] < 0 or pt[i] >= self.size[i] - 1: oob = True
          if oob: break
          if np.array_equiv(cur_box, last_box): next
          last_box = cur_box
          # calculate the intensity of each point on the box
          for bp in self.BOX_POINTS + cur_box:
            d = 0
            w1 = bp - pt1
            w2 = bp - pt2
            if np.dot(w1, v) <= 0:
              d = np.linalg.norm(w1)
            elif np.dot(w2, v) >= 0:
              d = np.linalg.norm(w2)
            else:
              d = np.linalg.norm(np.cross(u, w1))
            if d <= self.AA_WIDTH:
              d /= self.AA_WIDTH
              bp = (bp[2], bp[1], bp[0])
              self.frame[bp] = max(self.frame[bp], (1-d)*255)
        pt1 = pt2

  def output_frame(self):
    self.out_file.write(self.frame)
    self.clear_frame()


class Interpolator(object):
  def __init__(self, obj, attr, start, end, dur):
    self.obj = obj
    self.attr = attr
    self.start = start
    self.range = end - start
    self.frames = int(np.ceil(dur * FRAMERATE))

  def at_frame(self, f):
    if f > self.frames: return False
    self.obj.__setattr__(self.attr, self._interp(f))
    return True


class Lerp(Interpolator):
  def _interp(self, f):
    return float(f)/self.frames * self.range + self.start


class Controller(object):
  def __init__(self, frame_plotter):
    self.interps = []
    self.objects = []
    self.frame = 0
    self.plotter = frame_plotter

  def add_tween(self, interp):
    self.interps.append(interp)

  def add_object(self, obj):
    self.objects.append(obj)

  def at_frame(self, f):
    self.frame = f
    self._propagate()

  def next_frame(self):
    self.frame += 1
    self._propagate()

  def _propagate(self):
    for i in self.interps:
      if not i.at_frame(self.frame):
        self.interps.remove(i)

  def animate(self):
    while True:
      for i in self.objects:
        i.draw()
      self.plotter.output_frame()
      self.next_frame()
      if len(self.interps) == 0: break
    self.frame = 0


class Model(object):
  origin, xaxis, yaxis, zaxis = [0, 0, 0], [1, 0, 0], [0, 1, 0], [0, 0, 1]

  def __init__(self, plotter):
    self.xrot = self.yrot = self.zrot = 0.0
    self.x = self.y = self.z = 0.0
    self.scale_factor = 1.0
    self.scale_direction = None
    self.plotter = plotter

  def draw(self, polys):
    self._refresh_matrix()
    if polys[0][0].size != 4:
      polys = [np.append(p, np.ones((p.shape[0], 1)), axis=1) for p in polys]
    t = self.matrix.T
    self.plotter.draw(np.dot(polys, t))

  def _refresh_matrix(self):
    self.matrix = transformations.concatenate_matrices(
                    transformations.translation_matrix([self.x, self.y, self.z]),
                    transformations.rotation_matrix(self.xrot, self.xaxis),
                    transformations.rotation_matrix(self.yrot, self.yaxis),
                    transformations.rotation_matrix(self.zrot, self.zaxis),
                    transformations.scale_matrix(self.scale_factor, self.origin, self.scale_direction))


class Shape(object):
  def __init__(self,plotter):
    self.plotter = plotter
    self.polygons = None

  def draw(self):
    self.plotter.draw(self.polygons)


class Cube(Shape):
  def __init__(self, plotter, size):
    super(Cube, self).__init__(plotter)
    self.polygons = np.empty((6,4,3))
    size = size/2.0
    d1 = np.array([-1., -1., 1., 1.]) * size
    d2 = np.array([-1., 1., 1., -1.]) * size
    u = np.array([1., 1., 1., 1.]) * size
    self.polygons = [
      # YZ planes
      np.array([u, d1, d2]).T,
      np.array([-u, d1, d2]).T,
      # XZ planes
      np.array([d1, u, d2]).T,
      np.array([d1, -u, d2]).T,
      # XY planes
      np.array([d1, d2, u]).T,
      np.array([d1, d2, -u]).T
    ]

