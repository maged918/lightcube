#!/usr/bin/env python
from animation_framework import *

mp = MatrixPlotter("anim1.raw")
controller = Controller(mp)

model = Model(mp)
model.z = model.x = model.y = 3

cube = Cube(model, 2)
controller.add_object(cube)

controller.add_tween(Lerp(model, "x", 0, 5, 5))
controller.add_tween(Lerp(model, "y", 0, 5, 5))
controller.add_tween(Lerp(model, "z", 0, 5, 5))
controller.animate()

controller.add_tween(Lerp(model, "x", 0, 5, 5))
controller.add_tween(Lerp(model, "y", 0, 5, 5))
controller.add_tween(Lerp(model, "z", 0, 5, 5))
controller.add_tween(Lerp(model, "scale_factor", 1,  3, 5))

#controller.add_tween(Lerp(model, "zrot", 0.0,  40.0, 100))
#controller.add_tween(Lerp(model, "yrot", 0.0,  40.0, 100))
#controller.add_tween(Lerp(model, "xrot", 0.0,  40.0, 100))

#controller.add_tween(Lerp(model, "y", 0, 5, 1))
controller.animate()
