#!/usr/bin/env python2

from distutils.core import setup, Extension
import numpy
import sys

sys.argv += ["build_ext", "--inplace"]

setup(name='_transformations', ext_modules=[
      Extension('_transformations', ['transformations.c'],
      include_dirs=[numpy.get_include()], extra_compile_args=[])],)

