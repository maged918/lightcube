`timescale 1ns/1ns

module double_buffer_tb();
  wire [7:0] q;
  reg [7:0] d;
  reg [8:0] ra, wa;
  reg flip = 0, we = 0;

  reg clk = 0;
  always #10 clk <= !clk;
  always #50 flip <= !flip;

  double_buffer db(clk, d, q, wa, ra, choose, we);

  initial begin
    $dumpfile("double_buffer_tb.lxt");
    $dumpvars(0, double_buffer_tb);
    ra = 5; wa = 5;
    we = 1;
    d = 1;
    #11;
    we = 0;
    #9999 $stop();
  end
endmodule
