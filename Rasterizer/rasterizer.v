`include "globals.v"

module rasterizer(frclk, clk, fast_clk, func, reset, matrix_position,value,
layer_enable,shift_register_input,latch_enable, shift_register_enable,ready);
  input                     frclk;                // Frame Rate Clock
  input                     clk;             // reduced shift register clock
  input                     fast_clk;      // Global 50MHz Clock
  input                     reset;
  output                    ready;
  input [4:0]               func;                 // Requested Function Index
  input [`CUBOID_LENGTH:0]  matrix_position;      // Input Voxel Position
  input [`VOXEL_DEPTH-1:0]  value;               // Input Voxel Value
  
  reg   [`VOXEL_DEPTH-1:0]         matrix_zero [`CUBOID_SIZE-1:0];        
  reg   [`VOXEL_DEPTH-1:0]         matrix_one [`CUBOID_SIZE-1:0];
  
  
  //wire `MATRIX_INDEX       (addr_in);            // Matrix address from input position
  //matrix_addr a1(addr_in, matrix_position);

  // General addressing unit
  
  wire [$clog2(`CUBOID_LENGTH)-1:0] x = matrix_position[8:6];
  wire [$clog2(`CUBOID_WIDTH)-1:0]  y = matrix_position[5:3];
  wire [$clog2(`CUBOID_HEIGHT)-1:0] z = matrix_position[2:0];
  
  assign frame_reset = ^clear_matrix_at_frame;
  
  wire[$clog2(`CUBOID_SIZE)-1:0] address;
  //assign address = {z,y,x};
  matrix_addr a2(address, x, y, z);
  
  
  reg current_input_matrix; //Bit to identify which matrix is used for current frame writing.
  reg ready;
  
  //Rasterizer output io
  parameter pwm = 255;

  output  [`CUBOID_HEIGHT-1:0] layer_enable; // Which layer is enabled, its corresponding value in the vector will be set to 1.
  output [`CUBOID_LENGTH-1:0] shift_register_input; //The Sin for each shift register. (Value to be shifted)
  output latch_enable; // one enable for all latches in a layer
  output shift_register_enable; //always set to 1, we are always shifting on the 8 shift registers, this signal is connected to the enable of all 8 shift registers


  reg [$clog2(`CUBOID_HEIGHT)-1:0] layers; // The decimal value of the layer enabled now (zero based)
  reg [`CUBOID_WIDTH:0] cube_bit; // the next bit to output on the next shift register.
  reg [`VOXEL_DEPTH -1:0] pwm_counter;
  reg [$clog2(`CUBOID_WIDTH)-1:0] latch_counter;
  //reg [`CUBOID_HEIGHT-1:0] layer_enable;
  wire [`CUBOID_LENGTH-1:0] shift_register_input;
  reg [$clog2(`CUBOID_LENGTH)-1:0] i;
  
  reg [`CUBOID_LENGTH-1:0] index;
  
  reg [1:0] clear_matrix_at_frame;
  reg [511:0] matrix_one_valid;
  reg [511:0] matrix_zero_valid;
  
  assign latch_enable = latch_counter == 0;
  assign shift_register_enable = 1; //shift register enable is always set to 1

  initial begin
    layers = 0;
    cube_bit = (layers+1)*`MATRIX_LAYER_SIZE- 1;
    latch_counter = 0;
    ready = 1;
    clear_matrix_at_frame = 0;
  end
  
  always @ (posedge clk) begin
    if(frame_reset) begin
	      clear_matrix_at_frame[0] <= ~ clear_matrix_at_frame[0];
    end
  end
  always @ (posedge fast_clk or negedge reset or posedge frame_reset) begin    
    if(!reset) begin
        matrix_one_valid <= 512'd0;
        matrix_zero_valid <= 512'd0;
        ready <= 1;
    end
    else if(frame_reset) begin
	      if (current_input_matrix == 0) begin
            matrix_zero_valid <= 512'd0;
	      end
	      else begin
	          matrix_one_valid <= 512'd0;
	      end
			ready <= 1;
    end
    else begin // FIXME we are skipping a clock here
      case (func)
        
      `RASTER_POINTS: begin
          if(current_input_matrix == 0) begin
              matrix_zero[address] <= value;
              matrix_zero_valid[address] <= 1;
          end
          else begin
              matrix_one[address] <= value;
              matrix_one_valid[address] <= 1;        
          end  
          ready <=1'b1;
        end
        
      `RASTER_LINES: begin
          ready <= 1'b0;
          //do_line_raster
        end
        default: ready <= 1'b1;

        endcase
    end
  end
  
  always @ (negedge frclk or negedge reset) begin
    if(~reset) begin
        current_input_matrix <= 0;
    end
	 else begin
		current_input_matrix <= ~current_input_matrix;
	 end
  end
  
  // HERE MARKS THE BEGINNING OF THE RASTERIZER OUTPUT 
  assign shift_register_input[0] = current_input_matrix ? (matrix_zero[cube_bit- (`CUBOID_LENGTH-1-0)] > pwm_counter) & matrix_zero_valid[cube_bit- (`CUBOID_LENGTH-1-0)]:
										(matrix_one[cube_bit- (`CUBOID_LENGTH-1-0)] > pwm_counter) & matrix_one_valid[cube_bit- (`CUBOID_LENGTH-1-0)];
  assign shift_register_input[1] = current_input_matrix ? (matrix_zero[cube_bit- (`CUBOID_LENGTH-1-1)] > pwm_counter) & matrix_zero_valid[cube_bit- (`CUBOID_LENGTH-1-1)] :
										(matrix_one[cube_bit- (`CUBOID_LENGTH-1-1)] > pwm_counter) & matrix_one_valid[cube_bit- (`CUBOID_LENGTH-1-1)];
  assign shift_register_input[2] = current_input_matrix ? (matrix_zero[cube_bit- (`CUBOID_LENGTH-1-2)] > pwm_counter) & matrix_zero_valid[cube_bit- (`CUBOID_LENGTH-1-2)]:
										(matrix_one[cube_bit- (`CUBOID_LENGTH-1-2)] > pwm_counter) & matrix_one_valid[cube_bit- (`CUBOID_LENGTH-1-2)];							
  assign shift_register_input[3] = current_input_matrix ? (matrix_zero[cube_bit- (`CUBOID_LENGTH-1-3)] > pwm_counter) & matrix_zero_valid[cube_bit- (`CUBOID_LENGTH-1-3)]:
										(matrix_one[cube_bit- (`CUBOID_LENGTH-1-3)] > pwm_counter) & matrix_one_valid[cube_bit- (`CUBOID_LENGTH-1-3)];
  assign shift_register_input[4] = current_input_matrix ? (matrix_zero[cube_bit- (`CUBOID_LENGTH-1-4)] > pwm_counter) & matrix_zero_valid[cube_bit- (`CUBOID_LENGTH-1-4)]:
										(matrix_one[cube_bit- (`CUBOID_LENGTH-1-4)] > pwm_counter) & matrix_one_valid[cube_bit- (`CUBOID_LENGTH-1-4)];
  assign shift_register_input[5] = current_input_matrix ? (matrix_zero[cube_bit- (`CUBOID_LENGTH-1-5)] > pwm_counter) & matrix_zero_valid[cube_bit- (`CUBOID_LENGTH-1-5)]:
										(matrix_one[cube_bit- (`CUBOID_LENGTH-1-5)] > pwm_counter) & matrix_one_valid[cube_bit- (`CUBOID_LENGTH-1-5)];
  assign shift_register_input[6] = current_input_matrix ? (matrix_zero[cube_bit- (`CUBOID_LENGTH-1-6)] > pwm_counter) & matrix_zero_valid[cube_bit- (`CUBOID_LENGTH-1-6)]:
										(matrix_one[cube_bit- (`CUBOID_LENGTH-1-6)] > pwm_counter) & matrix_one_valid[cube_bit- (`CUBOID_LENGTH-1-6)];
  assign shift_register_input[7] = current_input_matrix ? (matrix_zero[cube_bit- (`CUBOID_LENGTH-1-7)] > pwm_counter) & matrix_zero_valid[cube_bit- (`CUBOID_LENGTH-1-7)]:
										(matrix_one[cube_bit- (`CUBOID_LENGTH-1-7)] > pwm_counter) & matrix_one_valid[cube_bit- (`CUBOID_LENGTH-1-7)];
										
  always @ (negedge clk or negedge reset or posedge frame_reset) begin
		if(~reset || frame_reset) begin
			latch_counter <= 0;
		end
		else
		  latch_counter <= latch_counter + 1;
  end
  
  always@(negedge clk or negedge reset or posedge frame_reset) begin
    if(~reset || frame_reset) begin
      cube_bit <= `MATRIX_LAYER_SIZE - 1;
    end
    else begin
      cube_bit <= cube_bit - `CUBOID_LENGTH;
      if(latch_counter == `CUBOID_WIDTH-1) begin
	      cube_bit <= (layers+1)*`MATRIX_LAYER_SIZE - 1; // we choose the last bit we are going to output, for the current
	      if(pwm_counter==pwm) begin
		      cube_bit <= (layers+2)*`MATRIX_LAYER_SIZE - 1;
	      end
      end
    end
  end

  always @ (negedge frclk) begin // Checks for posedge of frame clock.
    clear_matrix_at_frame[1] <= !clear_matrix_at_frame[1];
  end
  
  assign layer_enable = 1'b1 << layers;
  
  always @ (posedge latch_enable or negedge reset or posedge frame_reset) begin // Increment PWM counter every 8 shifts.
		if (!reset || frame_reset)
			pwm_counter <= 0;
		else
			if (pwm_counter == pwm)
				pwm_counter <= 0;
			else
				pwm_counter <= pwm_counter + 1;
  end
  
  always @ (posedge latch_enable or negedge reset or posedge frame_reset)  begin // Increments layer after each PWM cycle.
    if(!reset || frame_reset) begin
      layers <= 0;
    end
	 else if(pwm_counter==pwm) begin
		layers <= layers + 1;
	 end
  end
endmodule
