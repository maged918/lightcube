`include "globals.v"
module test_point (
  output wire [4:0] func,
  output reg [`CUBOID_LENGTH:0] matrix_position,
  output wire [`VOXEL_DEPTH-1:0] value,
  input clock_50,
  input [8:0] point,
  input reset,
  input frame_clock,
  input down,
  input clk
);

reg [1:0] pointer;
reg [8:0] points [3:0];

assign func = `RASTER_POINTS;
assign value = 8'd255;
parameter fps = 3'd5;
wire[20:0] CLK_DIV = 50000000 / fps;

wire[2:0] x = point[8:6];
wire[2:0] y = point[5:3];
wire[2:0] z = point[2:0];

reg move;

always @ (negedge reset or posedge frame_clock) begin
	if(~reset) begin
	//if (counter >= CLK_DIV) begin
    //clk <= !clk;
   // counter <= 0;
	 points[0] <= point;
	 points[1][8:6] <= x;
	 points[1][5:3] <= y;
	 points[1][2:0] <= z;
	 points[2][8:6] <= x;
	 points[2][5:3] <= y;
	 points[2][2:0] <= z;
	 points[3][8:6] <= x;
	 points[3][5:3] <= y;
	 points[3][2:0] <= z;
	 /*points[0] <= 000_000_000;
	 points[1] <= 000_000_001;
	 points[2] <= 000_000_010;
	 points[3] <= 000_000_011;*/
	end
	else begin
		if(~down) begin
		points[0] <= points[1];
		points[1] <= points[2];
		points[2] <= points[3];
		points[3][5:3] <= points[3][5:3]+1; 
		end
		else begin
		points[0] <= points[1];
		points[1] <= points[2];
		points[2] <= points[3];
		points[3][2:0] <= points[3][2:0]-1; 
		end
	end
  //end
   //else counter <= counter + 1;
end

always @ (posedge clk) begin
  pointer <= pointer+1;
  matrix_position <= points[pointer];
end

/*assign points[0] = point;
assign points[1] = 9'b000_000_001;
assign points[2] = 9'b000_001_010;
assign points[3] = 9'b000_010_011;
*/


endmodule


