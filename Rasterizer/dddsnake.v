`include "globals.v"
module dddsnake (
    output reg [4:0] func,
    output reg [`CUBOID_LENGTH:0] matrix_position,
    output reg [`VOXEL_DEPTH-1:0] value,
	 output reg frame_clock_out,
    input up, // +Z
    input down, // -Z
    input left, // -X
    input right, // +X
    input forwards, // +Y
    input backwards, // -Y
    input reset,
    input clock_50, 
    //input frame_clock
	 input play_clock
);
//  A    A 
// Y \   | Z      
//    \  |     
//     \ |      
//      \|      
//       +----------> X
             
reg [8:0] snake [127:0];
reg [511:0] occupied;
reg [8:0] apple;

reg [6:0] head;
reg [6:0] tail;
reg [6:0] draw_pointer;

reg [2:0] direction_x;
reg [2:0] direction_y;
reg [2:0] direction_z;

reg donot_move_tail;
reg [8:0] newapple;
reg bit_in;

reg [31:0] play_clock_count;
//reg play_clock; CHANGED
//assign play_clock = frame_clock;

// Signals
reg [1:0] reset_draw_pointer_signal;
reg [1:0] create_new_apple_signal;
//reg [1:0] frame_clock_signal;

// Status bits
reg draw;
reg draw_apple;
reg lose;

// initials
parameter initial_point_1 = 9'b111_100_100;
parameter initial_point_2 = 9'b110_100_100;
parameter initial_point_3 = 9'b101_100_100;
parameter start_apple = 9'b100_100_100;

initial
begin
head = 0;
tail = 0;
draw_pointer = 0;
end

// Send the snake to the rasterizer
/*
always @ (posedge clock_50, negedge reset)
begin
  if (~reset) begin
    reset_draw_pointer_signal[0] <= 0;
    draw <= 1;

    draw_apple <= 0;
    
    func <= `RASTER_NOP;
    value <= 8'h00;
    matrix_position <= 8'h00;
    
    draw_pointer <= 0;
    frame_clock_out <= 1;
  end
  else begin
    if(^reset_draw_pointer_signal) begin
      reset_draw_pointer_signal[0] <= ~reset_draw_pointer_signal[0];
      draw_pointer <= tail;
      draw <= 1;
      frame_clock_out <= 1;
    end
    else
    if(draw_apple) begin
      func <= `RASTER_POINTS;
      matrix_position <= apple;
      value <= 8'hFF;
      draw_apple <= 0;
    end
    else
    if(draw) begin
      func <= `RASTER_POINTS;
      matrix_position <= snake[draw_pointer];
      value <= 8'h7F;
      if(draw_pointer == head) begin
        value <= 8'hFF;
        draw <= 0;
        draw_apple <= 1;
      end
      draw_pointer <= draw_pointer + 1;
    end
    else begin
      func <= `RASTER_NOP;
      value <= 8'h00;
      matrix_position <= 8'h00;
      frame_clock_out <= 0;
    end
  end
end
*/

always @ (negedge clock_50, negedge reset) begin
	if (~reset) begin
	 reset_draw_pointer_signal[0] <= 0;
	 func <= `RASTER_NOP;
	 value <= 8'h00;
	 matrix_position <= 8'h00;
	 draw_pointer <= 0;
	 frame_clock_out <= 1;
	end
	else begin
		if(^reset_draw_pointer_signal) begin
			reset_draw_pointer_signal[0] <= ~reset_draw_pointer_signal[0];
			draw_pointer <= 0;
			func <= `RASTER_POINTS;
			matrix_position <= apple;
			value <= 8'hFF;
			frame_clock_out <= 1;
		 end
		 else begin
		  matrix_position <= snake[draw_pointer];
			if (occupied[snake[draw_pointer]]) begin
				
				value[6:0] <= 7'h7F;
				value[7] <= draw_pointer == head;
			end
			else begin
				func <= `RASTER_NOP;
				value <= 8'h00;
				matrix_position <= 8'h00;
				frame_clock_out <= 0;
			end
			draw_pointer <= draw_pointer + 1;
		 end
	 end
end

// Create random apples
always @ (posedge clock_50 or negedge reset)
begin
  if (~reset) begin
    apple <= start_apple;
    create_new_apple_signal[0] <= 0;
  end
  else begin
    if(^create_new_apple_signal) begin
      bit_in <= ^(newapple ^ snake[tail]); // fix to check how random this is
      newapple <= {newapple[7:0], bit_in}; // get a random apple
      if(~occupied[{newapple[7:0], bit_in}]) begin
        create_new_apple_signal[0] <= ~create_new_apple_signal[0];
        apple <= {newapple[7:0], bit_in};
      end
    end
  end
end

always @ (posedge up or posedge down or posedge left or posedge right or posedge forwards or posedge backwards or negedge reset)
begin
	if(~reset) begin //ADDED TO HANDLE RESETTING
		  direction_x <= 3'b000;
        direction_y <= 3'b000;
        direction_z <= 3'b001;
	end else 
	if (up) begin
        direction_x <= 3'b000;
        direction_y <= 3'b000;
        direction_z <= 3'b001;
    end else
    if (down) begin
        direction_x <= 3'b000;
        direction_y <= 3'b000;
        direction_z <= 3'b111;
    end else
    if (left) begin
        direction_x <= 3'b111;
        direction_y <= 3'b000;
        direction_z <= 3'b000;
    end else
    if (right) begin
        direction_x <= 3'b001;
        direction_y <= 3'b000;
        direction_z <= 3'b000;
    end else
    if (forwards) begin
        direction_x <= 3'b000;
        direction_y <= 3'b001;
        direction_z <= 3'b000;
    end else
    if (backwards) 
	 begin
        direction_x <= 3'b000;
        direction_y <= 3'b111;
        direction_z <= 3'b000;
    end
end

// restart drawing
/*
always @ (posedge frame_clock or negedge reset)
begin
    if (~reset) begin
      reset_draw_pointer_signal[1] <= 0;
    end
    else begin
      reset_draw_pointer_signal[1] <= ~reset_draw_pointer_signal[1];
    end
end
*/

wire [8:0] new_head;
assign new_head[2:0] = snake[head][2:0] + direction_z;
assign new_head[5:3] = snake[head][5:3] + direction_y;
assign new_head[8:6] = snake[head][8:6] + direction_x;

// move the snake
always @ (posedge play_clock or negedge reset)
begin
    if (~reset) begin
      lose <= 0;
      occupied <= 512'd0;
		reset_draw_pointer_signal[1] <= 0;
      head <= 2;
      tail <= 0;
      snake[0] <= initial_point_1;
      occupied[initial_point_1] <= 1;
      snake[1] <= initial_point_2;
      occupied[initial_point_2] <= 1;
      snake[2] <= initial_point_3;
      occupied[initial_point_3] <= 1;
      donot_move_tail <= 0;
      create_new_apple_signal[1] <= 0;
    end
    else begin
		
      if(~lose) begin
        head <= head + 1; 
        if (!donot_move_tail) begin
            tail <= tail + 1;
				occupied[snake[tail]] <= 0;
        end
        else begin
            donot_move_tail <= 0;
        end
        snake[head + 1] <= new_head;
        if (occupied[new_head]) begin
            lose <= 1;
        end
        else if (new_head == apple) begin
            donot_move_tail <= 1;
            create_new_apple_signal[1] <= !create_new_apple_signal[1];
        end
        else begin
            occupied[new_head] <= 1;
        end
      end
	   reset_draw_pointer_signal[1] <= ~reset_draw_pointer_signal[1];
    end
end


/*always @ (posedge clock_50) begin
  play_clock_count <= play_clock_count +1;
  if (play_clock_count == 12500000) begin
    play_clock <= ~play_clock;
    play_clock_count <= 0;
  end
end/*REMOVED AND REPLACED ABOVE BY NORMAL FRAME CLOCK*/ 

endmodule
