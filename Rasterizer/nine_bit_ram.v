module nine_bit_ram (input clk,
                     output reg [8:0] q, input [8:0] d,
                     input [9:0] write_addr, input [9:0] read_addr,
                     input write_en);

  reg [8:0] mem [1023:0];

  always@(posedge clk) begin
    if (write_en) mem[write_addr] <= d;
    q <= mem[read_addr];
  end

endmodule
