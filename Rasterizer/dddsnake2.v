`include "globals.v"
module dddsnake2 (
  output reg [4:0] func,
  output reg [`CUBOID_LENGTH:0] matrix_position,
  output reg [`VOXEL_DEPTH-1:0] value,
  output reg frame_clock, //frame clock to the rasterizer
  input reset,
  input clk, 
  input up, // +Z
  input down, // -Z
  input left, // -X
  input right, // +X
  input forwards, // +Y
  input backwards // -Y
);
  
  wire [8:0] read_value;
  reg write_en = 0;
  reg [9:0] write_addr, read_addr;
  reg [8:0] write_value;
  nine_bit_ram snake_queue (clk, read_value, write_value,
                            write_addr, read_addr, write_en);
  
  parameter BASE_CLK = 50_000_000; // real
  //parameter BASE_CLK = 200; //testing
  parameter PPS = 2;
  parameter play_clock_counter_threshold = BASE_CLK / PPS;
  reg [31:0] play_clock_count = 0;
  reg play_clock = 0;
  
  always @ (posedge clk) begin
    play_clock_count <= play_clock_count +1;
    if (play_clock_count == play_clock_counter_threshold) begin
      play_clock <= 1;
      play_clock_count <= 0;
    end
    else begin
      play_clock <= 0;
    end
  end

  reg old_play_clock;
  reg play_clock_posedge;
  
  always@(posedge clk) begin
    old_play_clock <= play_clock;
    play_clock_posedge <= old_play_clock == 0 && play_clock == 1;
  end
  
  reg [511:0] occupied;
  reg [9:0] head_addr;
  reg [9:0] tail_addr;
  
  wire [8:0] new_head;
  reg [8:0] current_head;
  
  reg [8:0] apple;
  wire [8:0] new_apple;
  reg [2:0] apple_offset;
  
  reg [2:0] direction_x;
  reg [2:0] direction_y;
  reg [2:0] direction_z;

  wire [8:0] initial_points [4:0];
  assign initial_points[0] = 9'b000_000_000;
  assign initial_points[1] = 9'b000_000_001;
  assign initial_points[2] = 9'b000_000_010;
  assign initial_points[3] = 9'b000_000_011;
  assign initial_points[4] = 9'b000_000_100; // FIXME RAGA3 EL APPLE ZAY MAKANET
  reg[2:0] initial_points_pointer; 
  
  assign new_head[2:0] = read_value[2:0] + direction_z;
  assign new_head[5:3] = read_value[5:3] + direction_y;
  assign new_head[8:6] = read_value[8:6] + direction_x;
  
  assign new_apple[2:0] = read_value[2:0] + apple_offset;
  assign new_apple[5:3] = read_value[5:3] + apple_offset;
  assign new_apple[8:6] = read_value[8:6] + apple_offset;
  
  assign snake_eats_apple = new_head == apple;
  
  parameter STATE_WAIT_FOR_PLAYCLOCK = 0;
  parameter STATE_CHANGE_HEAD = 1;
  parameter STATE_GENERATE_APPLE = 2;
  parameter STATE_DRAW_SNAKE = 3;
  parameter STATE_DRAW_HEAD = 4;
  parameter STATE_DRAW_APPLE = 5;
  parameter STATE_FRAME_POSEDGE = 6;
  parameter STATE_INITIALIZATION = 7;
  parameter STATE_CHECK_LOSS = 8;
  parameter STATE_LOST = 9;
  parameter STATE_UNOCCUPY_OLD_TAIL = 10;
  parameter STATE_PAUSED = 11;
  parameter STATE_INITIALIZE_DRAW_SNAKE = 12;
  parameter STATE_OCCUPY_NEW_HEAD = 13;
  
  reg [9:0] state;
  
  always @ (negedge clk or negedge reset) begin
    if (~reset) begin
      //state <= STATE_WAIT_FOR_PLAYCLOCK;
      state <= STATE_INITIALIZATION;
      write_addr <= 0;
      write_value <= 0;
      write_en <= 0;
      initial_points_pointer <=0;
      read_addr <= 0;
      tail_addr <= 0;
      head_addr <= 3;
		letter_enable <= 1;
      current_head <= initial_points[3];
      apple <= 0;//initial_points[4];
      apple_offset <= 4;
      occupied <= 512'd0;
      func <= `RASTER_NOP;
      value <= 8'h00;
      matrix_position <= 8'h00;
      frame_clock <= 0;
      old_direction_state <= STATE_NOT_MOVING;
    end
    else begin
      case (state)
        STATE_INITIALIZATION: begin
          initial_points_pointer <= initial_points_pointer+1;
          if(initial_points_pointer != 4) begin
            write_addr <= initial_points_pointer;
            write_value <= initial_points[initial_points_pointer];
            occupied[initial_points[initial_points_pointer]] <= 1;
            write_en <= 1;
				letter_enable <=1;
          end
          else begin
            apple <= initial_points[initial_points_pointer];
            write_en <= 0;
            initial_points_pointer <= 0;
            state <= STATE_INITIALIZE_DRAW_SNAKE;
				letter_enable <=1;
          end
        end
        STATE_WAIT_FOR_PLAYCLOCK: begin
          if(play_clock_posedge) begin
            if(direction_state == STATE_NOT_MOVING) begin
              state <= STATE_INITIALIZE_DRAW_SNAKE;
            end
            else begin
              state <= STATE_CHANGE_HEAD;
              read_addr <= head_addr; // read the current head position
              old_direction_state <= direction_state;
            end
          end
			 letter_enable <= 1;
        end
        STATE_CHANGE_HEAD: begin
          head_addr <= head_addr + 1;
          write_addr <= head_addr + 1; // write in the new head position
          write_value <= new_head;
          current_head <= new_head; // move to own always block, or not :)
          write_en <= 1;
          if(!snake_eats_apple) begin //FIXME: CHANGE TO REGISTER
            tail_addr <= tail_addr + 1; 
            state <= STATE_UNOCCUPY_OLD_TAIL;
            read_addr <= tail_addr;
          end
          else begin 
            // tail doesnt move so no update to occupied or tail pointer
            // the apple never appears on the snake so no loss check either
            state <= STATE_GENERATE_APPLE;
            occupied[new_head] <= 1;
            read_addr <= tail_addr;
          end
        end
        STATE_UNOCCUPY_OLD_TAIL: begin
          occupied[read_value] <= 0; // unoccupy the previous tail
          state <= STATE_CHECK_LOSS;
        end
        STATE_CHECK_LOSS: begin
          if(occupied[current_head]) begin
            state <= STATE_LOST;
          end
          else begin
            state <= STATE_OCCUPY_NEW_HEAD;
          end
        end
        STATE_OCCUPY_NEW_HEAD: begin
          occupied[current_head] <= 1;
          state <= STATE_INITIALIZE_DRAW_SNAKE;
        end
        STATE_GENERATE_APPLE: begin
          apple <= new_apple;
          if (!occupied[new_apple]) begin
            state <= STATE_INITIALIZE_DRAW_SNAKE;
            apple_offset <= 4; //reset apple offset
          end
          else begin
            apple_offset <= apple_offset + 1;
          end
        end
        STATE_INITIALIZE_DRAW_SNAKE: begin
          read_addr <= tail_addr;
          state <= STATE_DRAW_SNAKE;
          frame_clock <= 0;
        end
        STATE_DRAW_SNAKE: begin
          func <= `RASTER_POINTS;
          matrix_position <= read_value;
          read_addr <= read_addr + 1;
          if (head_addr == read_addr) begin
            state <= STATE_DRAW_APPLE;
            value <= 8'hFF;
          end
          else begin
            value <= 8'h0F;
          end
        end
        STATE_DRAW_APPLE: begin
          func <= `RASTER_POINTS;
          value <= 8'hFF;
          matrix_position <= apple;
          state <= STATE_FRAME_POSEDGE;
        end
        STATE_FRAME_POSEDGE: begin
          func <= `RASTER_NOP;
          value <= 8'h00;
          matrix_position <= 8'h00;
          frame_clock <= 1;
          state <= STATE_WAIT_FOR_PLAYCLOCK;
        end
        STATE_LOST: begin
          // you have lost wait for reset
			 letter_enable <=0;
			 frame_clock <= write_frame_clock;
			 func <= letter_func;
			 value <= letter_val;
			 matrix_position <= letter_point;
        end
        STATE_PAUSED: begin
          // leave the pause state on unpause
        end
      endcase
    end
  end
  //LETTERS CODE
  assign letters[0] = 8'h0A;
  assign letters[1] = 8'h0B;
  assign letters[2] = 8'h0C;
  assign letters[3] = 8'h0D;
  
  wire [7:0] letters [3:0];
  wire [8:0] letter_point;
  write_letter write(clk,letters[letter_pointer],letter_enable,write_frame_clock,
		letter_point,letter_bit,letter_func,letter_val,end_letter_signal);
  reg [1:0] letter_pointer;
  wire [8:0] letter_val;
  wire [7:0] letter_func;
  reg letter_enable =1;
  wire letter_bit;
  wire end_letter_signal;
  wire write_frame_clock;
  always @(posedge end_letter_signal) begin
	letter_pointer <= letter_pointer +1;
  end
  //END LETTERS CODE
  reg [9:0] old_direction_state;
  reg [9:0] direction_state;
  parameter STATE_MOVING_UP = 0;
  parameter STATE_MOVING_DOWN = 1;
  parameter STATE_MOVING_LEFT = 2;
  parameter STATE_MOVING_RIGHT = 3;
  parameter STATE_MOVING_FORWARDS = 4;
  parameter STATE_MOVING_BACKWARDS = 5;
  parameter STATE_NOT_MOVING = 6;
  
  always @ (posedge clk or negedge reset) begin
    if(!reset) begin
      direction_state <= STATE_NOT_MOVING;
    end
    else begin
      if(up && old_direction_state != STATE_MOVING_DOWN)
			direction_state <= STATE_MOVING_UP;
		else if(down && old_direction_state != STATE_MOVING_UP)
			direction_state <= STATE_MOVING_DOWN;
		else if(left && old_direction_state != STATE_MOVING_RIGHT)
			direction_state <= STATE_MOVING_LEFT;
		else if(right && old_direction_state != STATE_MOVING_LEFT)
			direction_state <= STATE_MOVING_RIGHT;
		else if(forwards && old_direction_state != STATE_MOVING_BACKWARDS)
			direction_state <= STATE_MOVING_FORWARDS;
		else if(backwards && old_direction_state != STATE_MOVING_FORWARDS)
			direction_state <= STATE_MOVING_BACKWARDS;
    end
  end
  
  always@(direction_state) begin
    case (direction_state)
      STATE_MOVING_UP: begin
        direction_x <= 3'b000;
        direction_y <= 3'b000;
        direction_z <= 3'b001;
      end
      STATE_MOVING_DOWN: begin
        direction_x <= 3'b000;
        direction_y <= 3'b000;
        direction_z <= 3'b111;
      end
      STATE_MOVING_LEFT: begin
        direction_x <= 3'b111;
        direction_y <= 3'b000;
        direction_z <= 3'b000;
      end
      STATE_MOVING_RIGHT: begin
        direction_x <= 3'b001;
        direction_y <= 3'b000;
        direction_z <= 3'b000;
      end
      STATE_MOVING_FORWARDS: begin
        direction_x <= 3'b000;
        direction_y <= 3'b001;
        direction_z <= 3'b000;
      end
      STATE_MOVING_BACKWARDS: begin
        direction_x <= 3'b000;
        direction_y <= 3'b111;
        direction_z <= 3'b000;
      end
      default: begin // STATE_NOT_MOVING
        direction_x <= 3'b000;
        direction_y <= 3'b000;
        direction_z <= 3'b000;
      end
    endcase
  end
  
endmodule
