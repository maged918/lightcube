`timescale 1us/1us
`include "globals.v"

module unit_test_rasterizer;

  reg reset = 0;
  reg [4:0] func = 0;
  reg [8:0] matrix_position = 0;
  wire [7:0] value = 8'd255;
  wire [7:0] shift_register_input, layer_enable, voxel_value;
  reg[5:0] i;
  initial begin
    $dumpfile("unit_test_rasterizer.lxt");
    $dumpvars(0, unit_test_rasterizer);
		reset = 1;
    #10 reset = 0;
    #10 reset = 1;
    #10 
    func = 1;
    matrix_position = {3'b000,3'b000,3'b000};
    /*
    #20 matrix_position = {3'b001,3'b111,3'b000};
    #20 matrix_position = {3'b010,3'b111,3'b000};
    #20 matrix_position = {3'b011,3'b111,3'b000};
    #20 matrix_position = {3'b100,3'b111,3'b000};
    #20 matrix_position = {3'b101,3'b111,3'b000};
    #20 matrix_position = {3'b110,3'b111,3'b000};
    #20 matrix_position = {3'b111,3'b111,3'b000};
    */
    #400000
    matrix_position = {3'b111,3'b111,3'b100};
    # 9999999 $stop;
  end

  reg clk = 0;
  reg frclk = 0;

  always #10 clk = !clk;
  always #400000 frclk = !frclk;

  rasterizer A(frclk, clk, func,reset, matrix_position,value,layer_enable,shift_register_input,
latch_enable,shift_register_enable,ready);

endmodule

