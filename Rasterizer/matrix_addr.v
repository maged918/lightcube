`include "globals.v"

module matrix_addr
(
  output [$clog2(`CUBOID_SIZE)-1:0] index,
  input  [`MATRIX_X_WIDTH-1:0]     x,
  input  [`MATRIX_Y_WIDTH-1:0]     y,
  input  [`MATRIX_Z_WIDTH-1:0]     z
);

`ifdef IS_POWER_OF_2_CUBE
  assign index = {z,y,x};
`else
  assign index = z*`CUBOID_LENGTH*`CUBOID_HEIGHT + y*`CUBOID_LENGTH + x;
`endif

endmodule
/*
reg [7:0] mat [511:0];
wire index;
matrix_addr m1(index, x, y, z);
val = mat[index];
*/
