`include "globals.v"
`define LAYER(i) layer_`i

module matrix_mem_8line
(
  output [`VOXEL_DEPTH-1:0] x_line [7:0],
  input [`MATRIX_Z_WIDTH-1:0] layer,
  input [`LAYER_INDEX_WIDTH-1:0] index
);

  genvar i;
  generate for (i=0; i<8; i++)
    reg [`VOXEL_DEPTH-1:0] LAYER(i) [7:0];
    assign x_line[i] = LAYER(i)[index];
  endgenerate

endmodule

