`timescale 1us/1us
`include "globals.v"

module unit_test_matrix_addr;

  reg `MATRIX_MEM(mem);

  reg [`MATRIX_X_BITS-1:0] x;
  reg [`MATRIX_Y_BITS-1:0] y;
  reg [`MATRIX_Z_BITS-1:0] z;

  wire [`MATRIX_ADDR_BITS-1:0] addr;

  matrix_addr m(addr, x, y, z);
  assign read = mem[addr];

  initial begin
    x = 1; y = 2; z = 1;
    #1;
    x = 3; z = 4;
    #1;
    y = 1;
    #1;
    x = 7; y = 2; z = 3;
    #2;
  end
endmodule

