// Test the rasterizer by drawing a point with co-ordinates taken from the switches
module main(input CLOCK_50,
                       input [17:0] SW, input [3:0] KEY,
                       output [35:0] GPIO);

  assign reset = KEY[0];

  assign GPIO[31] = 1;
  assign GPIO[3] = 0;
  assign GPIO[1] = 1;

  wire [7:0] shift_b, layer_out;
  assign {GPIO[25], GPIO[23], GPIO[21], GPIO[19],
          GPIO[17], GPIO[15], GPIO[13], GPIO[11]} = shift_b[7:0];
  assign {GPIO[24], GPIO[22], GPIO[20], GPIO[18],
          GPIO[16], GPIO[14], GPIO[12], GPIO[10]} = layer_out[7:0];

  wire [8:0] rastr_pos;
  
  assign up = SW[5];
  assign down = SW[4];
  assign left = SW[3];
  assign right = SW[2];
  assign forwards = SW[1];
  assign backwards = SW[0];
  
  wire [7:0] letter [3:0];

  assign letter[0] = 8'h0A;
  assign letter[1] = 8'h0B;
  assign letter[2] = 8'h0C;
  assign letter[3] = 8'h0D;
  reg [1:0] letter_pointer = 0;
  
  always@(posedge end_letter_signal) begin
	letter_pointer <= letter_pointer +1;
  end
  
  wire letter_enable;
  wire rastr_val_bit;
  wire end_letter_signal;
  wire write_frame_clock;
  write_letter write(CLOCK_50,letter[letter_pointer],1,
		frclk,rastr_pos,rastr_val_bit,rastr_func,rastr_val,end_letter_signal);
		
	/*always@(posedge CLOCK_50) begin
		if(rastr_val_bit == 1) begin
			rastr_val <= 8'hFF;
			rastr_func <= `RASTER_POINTS;
		end
		else begin
			rastr_val <= 8'h00;
			rastr_func <= `RASTER_NOP;
		end
	end */
	
	wire [7:0] rastr_val;
	wire [4:0] rastr_func;

  rasterizer rastr(CLOCK_50, ready, frclk,
                   0, rastr_func, 0,
                   rastr_pos, rastr_val,
                   GPIO[33], GPIO[35],
                   shift_b, layer_out,
                   reset); 
  /*dddsnake2 snake(rastr_func, rastr_pos, rastr_val,
                  frclk, reset, CLOCK_50,
                  up, down, left, right,
                  forwards, backwards); */
                  
endmodule

