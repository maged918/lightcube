`include "globals.v"
module write_letter(clk,letter,letter_enable,frame_clock,rastr_point,rastr_bit,rastr_func,rastr_value,end_letter);

input clk;
input [7:0] letter;
input letter_enable;
output reg end_letter;
reg[2:0] plane_counter;
output reg[8:0] rastr_point = 0;
output reg rastr_bit = 0;
output reg [7:0] rastr_value = 8'h00;
output reg [4:0] rastr_func = `RASTER_NOP;

parameter BASE_CLK = 50_000_000; // real
parameter FPS = 8;
parameter frame_clock_counter_threshold = BASE_CLK / FPS;
parameter frame_clock_counter_threshold_diff = (BASE_CLK / FPS) -1;
reg [31:0] frame_clock_count = 0;
output reg frame_clock = 0;
reg frame_clock_diff = 0;
 
always @ (posedge clk or negedge letter_enable) begin
	if(~letter_enable) begin
		frame_clock_count <=0;
		frame_clock <=0;
	end
	else begin
		frame_clock_count <= frame_clock_count +1;
		if (frame_clock_count == frame_clock_counter_threshold) begin
			frame_clock <= 1;
			frame_clock_count <= 0;
		end
		else begin
			frame_clock <= 0;
		end
	end
end

always@(posedge frame_clock or negedge letter_enable) begin
	if(~letter_enable) begin
		plane_counter <= 0;
	end
	else begin
		plane_counter <= plane_counter +1;
	end
end

wire [63:0] letter_plane;
wire [63:0] letter_L;
wire [63:0] letter_O;
wire [63:0] letter_S;
wire [63:0] letter_E;
wire [63:0] letter_R;
wire [63:0] letter_line;


assign letter_L[16] = 1'b1;
assign letter_L[17] = 1'b1;
assign letter_L[24] = 1'b1;
assign letter_L[25] = 1'b1;
assign letter_L[32] = 1'b1;
assign letter_L[33] = 1'b1;
assign letter_L[40] = 1'b1;
assign letter_L[41] = 1'b1;
assign letter_L[48] = 1'b1;
assign letter_L[49] = 1'b1;
assign letter_L[56] = 1'b1;
assign letter_L[57] = 1'b1;

assign letter_L[0] = 1'b1;
assign letter_L[1] = 1'b1;
assign letter_L[2] = 1'b1;
assign letter_L[3] = 1'b1;
assign letter_L[4] = 1'b1;
assign letter_L[5] = 1'b1;
assign letter_L[6] = 1'b1;
assign letter_L[7] = 1'b1;
assign letter_L[8] = 1'b1;
assign letter_L[9] = 1'b1;
assign letter_L[10] = 1'b1;
assign letter_L[11] = 1'b1;
assign letter_L[12] = 1'b1;
assign letter_L[13] = 1'b1;
assign letter_L[14] = 1'b1;
assign letter_L[15] = 1'b1;


assign letter_O[2] = 1'b1;
assign letter_O[3] = 1'b1;
assign letter_O[4] = 1'b1;
assign letter_O[5] = 1'b1;
assign letter_O[9] = 1'b1;
assign letter_O[14] = 1'b1;
assign letter_O[16] = 1'b1;

assign letter_O[23] = 1'b1;
assign letter_O[24] = 1'b1;
assign letter_O[31] = 1'b1;
assign letter_O[32] = 1'b1;
assign letter_O[39] = 1'b1;
assign letter_O[40] = 1'b1;
assign letter_O[47] = 1'b1;

assign letter_O[49] = 1'b1;
assign letter_O[54] = 1'b1;
assign letter_O[58] = 1'b1;
assign letter_O[59] = 1'b1;
assign letter_O[60] = 1'b1;
assign letter_O[61] = 1'b1;

assign letter_plane[0] = 1'b1;
assign letter_plane[1] = 1'b1;
assign letter_plane[2] = 1'b1;
assign letter_plane[3] = 1'b1;
assign letter_plane[4] = 1'b1;
assign letter_plane[5] = 1'b1;
assign letter_plane[6] = 1'b1;
assign letter_plane[7] = 1'b1;
assign letter_plane[8] = 1'b1;
assign letter_plane[9] = 1'b1;
assign letter_plane[10] = 1'b1;
assign letter_plane[11] = 1'b1;
assign letter_plane[12] = 1'b1;
assign letter_plane[13] = 1'b1;
assign letter_plane[14] = 1'b1;
assign letter_plane[15] = 1'b1;
assign letter_plane[16] = 1'b1;
assign letter_plane[17] = 1'b1;
assign letter_plane[18] = 1'b1;
assign letter_plane[19] = 1'b1;
assign letter_plane[20] = 1'b1;
assign letter_plane[21] = 1'b1;
assign letter_plane[22] = 1'b1;
assign letter_plane[23] = 1'b1;
assign letter_plane[24] = 1'b1;
assign letter_plane[25] = 1'b1;
assign letter_plane[26] = 1'b1;
assign letter_plane[27] = 1'b1;
assign letter_plane[28] = 1'b1;
assign letter_plane[29] = 1'b1;
assign letter_plane[30] = 1'b1;
assign letter_plane[31] = 1'b1;
assign letter_plane[32] = 1'b1;
assign letter_plane[33] = 1'b1;
assign letter_plane[34] = 1'b1;
assign letter_plane[35] = 1'b1;
assign letter_plane[36] = 1'b1;
assign letter_plane[37] = 1'b1;
assign letter_plane[38] = 1'b1;
assign letter_plane[39] = 1'b1;
assign letter_plane[40] = 1'b1;
assign letter_plane[41] = 1'b1;
assign letter_plane[42] = 1'b1;
assign letter_plane[43] = 1'b1;
assign letter_plane[44] = 1'b1;
assign letter_plane[45] = 1'b1;
assign letter_plane[46] = 1'b1;
assign letter_plane[47] = 1'b1;
assign letter_plane[48] = 1'b1;
assign letter_plane[49] = 1'b1;
assign letter_plane[50] = 1'b1;
assign letter_plane[51] = 1'b1;
assign letter_plane[52] = 1'b1;
assign letter_plane[53] = 1'b1;
assign letter_plane[54] = 1'b1;
assign letter_plane[55] = 1'b1;
assign letter_plane[56] = 1'b1;
assign letter_plane[57] = 1'b1;
assign letter_plane[58] = 1'b1;
assign letter_plane[59] = 1'b1;
assign letter_plane[60] = 1'b1;
assign letter_plane[61] = 1'b1;
assign letter_plane[62] = 1'b1;
assign letter_plane[63] = 1'b1;

assign letter_line[0] =1'b1;
assign letter_line[1] =1'b1;
assign letter_line[2] =1'b1;
assign letter_line[3] =1'b1;
assign letter_line[4] =1'b1;
assign letter_line[5] =1'b1;
assign letter_line[6] =1'b1;
assign letter_line[7] =1'b1;

assign letter_S [1] =1'b1;
assign letter_S [2] =1'b1;
assign letter_S [3] =1'b1;
assign letter_S [12] =1'b1;
assign letter_S [20] =1'b1;
assign letter_S [27] =1'b1;
assign letter_S [35] =1'b1;
assign letter_S [42] =1'b1;
assign letter_S [50] =1'b1;
assign letter_S [59] =1'b1;
assign letter_S [60] =1'b1;
assign letter_S [61] =1'b1;

assign letter_E [0] =1'b1;
assign letter_E [1] =1'b1;
assign letter_E [2] =1'b1;
assign letter_E [3] =1'b1;
assign letter_E [4] =1'b1;
assign letter_E [5] =1'b1;
assign letter_E [6] =1'b1;
assign letter_E [7] =1'b1;
assign letter_E [8] =1'b1;
assign letter_E [9] =1'b1;
assign letter_E [10] =1'b1;
assign letter_E [11] =1'b1;
assign letter_E [12] =1'b1;
assign letter_E [13] =1'b1;
assign letter_E [14] =1'b1;
assign letter_E [15] =1'b1;
assign letter_E [24] =1'b1;
assign letter_E [25] =1'b1;
assign letter_E [26] =1'b1;
assign letter_E [27] =1'b1;
//assign letter_E [28] =1'b1;
//assign letter_E [29] =1'b1;
//assign letter_E [30] =1'b1;
//assign letter_E [31] =1'b1;
assign letter_E [32] =1'b1;
assign letter_E [33] =1'b1;
assign letter_E [34] =1'b1;
assign letter_E [35] =1'b1;
//assign letter_E [36] =1'b1;
//assign letter_E [37] =1'b1;
//assign letter_E [38] =1'b1;
//assign letter_E [39] =1'b1;
assign letter_E [48] =1'b1;
assign letter_E [49] =1'b1;
assign letter_E [50] =1'b1;
assign letter_E [51] =1'b1;
assign letter_E [52] =1'b1;
assign letter_E [53] =1'b1;
assign letter_E [54] =1'b1;
assign letter_E [55] =1'b1;
assign letter_E [56] =1'b1;
assign letter_E [57] =1'b1;
assign letter_E [58] =1'b1;
assign letter_E [59] =1'b1;
assign letter_E [60] =1'b1;
assign letter_E [61] =1'b1;
assign letter_E [62] =1'b1;
assign letter_E [63] =1'b1;
assign letter_E [16] =1'b1;
assign letter_E [17] =1'b1;
assign letter_E [40] =1'b1;
assign letter_E [41] =1'b1;

assign letter_R[0] =1'b1;
assign letter_R[5] =1'b1;
assign letter_R[8] =1'b1;
assign letter_R[12] =1'b1;
assign letter_R[16] =1'b1;
assign letter_R[19] =1'b1;
assign letter_R[24] =1'b1;
assign letter_R[26] =1'b1;
assign letter_R[32] =1'b1;
assign letter_R[33] =1'b1;
assign letter_R[34] =1'b1;
assign letter_R[35] =1'b1;
assign letter_R[36] =1'b1;
assign letter_R[40] =1'b1;
assign letter_R[45] =1'b1;
assign letter_R[48] =1'b1;
assign letter_R[53] =1'b1;
assign letter_R[56] =1'b1;
assign letter_R[57] =1'b1;
assign letter_R[58] =1'b1;
assign letter_R[59] =1'b1;
assign letter_R[60] =1'b1;

parameter L = 8'h0A;
parameter O = 8'h0B;
parameter S = 8'h0C;
parameter E = 8'h0D;
parameter R = 8'h0E;
//parameter Plane = 8'h00;
//parameter Line = 8'h0C;

reg [2:0] plane_x = 0;
wire [2:0] plane_y;
reg [2:0] plane_z = 0;
assign plane_y = plane_counter;

parameter STATE_INCREMENT_FRCLK = 0;
parameter STATE_DRAW_POINTS =1;
parameter STATE_WAIT_FOR_FRCLK = 2;
parameter STATE_CHANGE_END =3;

reg[9:0] state = STATE_DRAW_POINTS; 
reg[5:0] draw_counter =0;
reg[7:0] wait_counter =0;
reg[5:0] point =0;

reg[2:0] plane_end_letter = 0;
always @(negedge clk, negedge letter_enable) begin
	if(~letter_enable) begin
		rastr_point <=0;
		rastr_bit <=0;
		draw_counter <=0;
		plane_end_letter <= 0;
		state <= STATE_INCREMENT_FRCLK;
		
		rastr_value <= 8'h00;
		rastr_func <= `RASTER_NOP;
	end
	else begin
		case (state)
			STATE_CHANGE_END: begin
				end_letter<=1;
				state <= STATE_INCREMENT_FRCLK;
				point <= point +1;
			end
			STATE_INCREMENT_FRCLK: begin
				end_letter <= 0;
				draw_counter <= 0;
				wait_counter <= wait_counter +1;
				if(wait_counter == 127) begin
					state <= STATE_DRAW_POINTS;
					plane_x <=0;
					plane_z <=0;
				end
			end
			STATE_DRAW_POINTS: begin
				plane_x <= plane_x+1;
				if(plane_x == 7) begin
					plane_z <= plane_z+1;
				end
				case(letter)
					/*Line : begin
						rastr_bit <= letter_line[draw_counter];
						if(letter_line[draw_counter] == 1) begin
							rastr_value <= 8'hFF;
						end
						else begin
							rastr_value <= 8'h00;
						end
					end
					Plane: begin
						rastr_bit <= letter_plane[draw_counter];
						if(letter_plane[draw_counter] == 1) begin
							rastr_value <= 8'hFF;
						end
						else begin
							rastr_value <= 8'h00;
						end
					end*/
					S : begin
						rastr_bit <= letter_S[draw_counter];
						if(letter_S[draw_counter] == 1) begin
							rastr_value <= 8'hFF;
						end
						else begin
							rastr_value <= 8'h00;
						end
					end
					E: begin
						rastr_bit <= letter_E[draw_counter];
						if(letter_E[draw_counter] == 1) begin
							rastr_value <= 8'hFF;
						end
						else begin
							rastr_value <= 8'h00;
						end
					end
					L:	begin
						rastr_bit <=  letter_L[draw_counter];
						if(letter_L[draw_counter] == 1) begin
							rastr_value <= 8'hFF;
						end
						else begin
							rastr_value<= 8'h00;
						end
					end
					R: begin
						rastr_bit <= letter_R[draw_counter];
						if(letter_R[draw_counter] == 1) begin
							rastr_value <= 8'hFF;
						end
						else begin
							rastr_value <= 8'h00;
						end					
					
					end
					O: begin
						rastr_bit <= letter_O[draw_counter];
						if(letter_O[draw_counter] == 1) begin
							rastr_value <= 8'hFF;
						end
						else begin
							rastr_value <= 8'h00;
						end
					end
				endcase 
				/*if(point == draw_counter) begin
					rastr_bit <=1;
				end
				else begin
					rastr_bit <=0;
				end*/
				end_letter <= 0;
				
				rastr_point <= {draw_counter[2:0],plane_y[2:0],draw_counter[5:3]};
				rastr_func <= `RASTER_POINTS;
				
				draw_counter <= draw_counter + 1;
				if(draw_counter == 63) begin
					state <= STATE_WAIT_FOR_FRCLK;
				end
				else begin
					state <= STATE_DRAW_POINTS;
				end
			end
			STATE_WAIT_FOR_FRCLK: begin
				rastr_point <= 9'b000_000_000;
				rastr_func <= `RASTER_NOP;
				rastr_value <= 8'h00;
				if(frame_clock == 1) begin
					plane_end_letter <= plane_end_letter +1;
					if(plane_end_letter==7) begin
						state <= STATE_CHANGE_END;
					end
					else begin
						state <= STATE_INCREMENT_FRCLK;
					end
				end
				else begin
					state <= STATE_WAIT_FOR_FRCLK;
					wait_counter <= 0;
				end
				end_letter <= 0;
			end
		endcase
	end
end

endmodule