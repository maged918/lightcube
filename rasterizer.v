`include "globals.v"

module rasterizer(input clk, output ready, input frclk,
                  input [4:0] ctrl_flags, input [4:0] func, input [4:0] func_flags,
                  input [8:0] pos_in, input [7:0] value,
                  output reg shift_clk = 0, output latch_clk,
                  output reg [7:0] shift_out = 8'h0, output [7:0] layer_out,
                  input reset);

  parameter BASE_CLK = 50_000_000;
  parameter SHIFT_OUT_FREQ = 2_500_000;
  parameter CLK_DIV = (BASE_CLK) / SHIFT_OUT_FREQ - 1;
  parameter SHIFT_OUT_DELAY = CLK_DIV * 2;
  parameter SHIFT_OUT_HOLD = CLK_DIV / 2;
  parameter CLOCK_DELAY = 10;

  reg ready_sig = 0;

  reg matr_choose = 0;
  always@(posedge frclk) matr_choose <= !matr_choose;

  reg [8:0] matr_wa = 0;
  reg [7:0] matr_in;
  wire [7:0] matr_out;
  wire matr_we = 1;

  // matrix memory
  // operated with inverted clock to make it negedge triggered
  double_buffer dbmat(!clk,
                      matr_in, matr_out,
                      matr_wa, {matr_x, matr_y, matr_z},
                      matr_choose, matr_we);


  reg [31:0] clk_div = 0;
  reg [9:0] clk_delay = CLOCK_DELAY;
  always@(posedge clk or negedge reset) begin
    if (!reset) begin
      clk_div <= 0;
      shift_clk <= 0;
      clk_delay <= CLOCK_DELAY;
    end
    else begin
      if (clk_delay != 0) clk_delay <= clk_delay - 1;
      else begin
        if (clk_div >= CLK_DIV) begin
          shift_clk <= !shift_clk;
          clk_div <= 0;
        end
        else clk_div <= clk_div + 1;
      end
    end
  end

  reg [2:0] shift_cnt = 3'h0;
  reg [2:0] layer = 3'h0;
  always@(posedge shift_clk) shift_cnt <= shift_cnt + 1;
  assign latch_clk = (shift_cnt == 0) & (!shift_clk);
  assign layer_clk = (pwm_count == 255);

  always@(posedge layer_clk) layer <= layer + 1;
  assign layer_out = 8'h1 << layer;
  assign pwm_clk = latch_clk;

  wire [7:0] pwm_value = 60;

  reg [7:0] pwm_count = 0;
  always@(posedge pwm_clk or negedge reset) begin
    if (!reset)
	   pwm_count <= 0;
    else
	   pwm_count <= pwm_count + 1;
  end

  // Shift Register Output
  reg old_shift_clk = 0, prep_next = 0;
  always@(posedge clk) old_shift_clk <= shift_clk;
  always@(posedge clk) prep_next <= (!old_shift_clk & shift_clk) | !reset;

  reg shift_out_ready = 0;
  reg [9:0] shift_out_hold = SHIFT_OUT_HOLD;
  reg [2:0] matr_x = 0, matr_y = 0;
  wire [2:0] matr_z;
  always@(posedge clk or negedge reset) begin
    if (!reset) begin
      matr_x <= 0;
      shift_out <= 0;
      shift_out_hold <= SHIFT_OUT_HOLD;
      shift_out_ready <= 0;
    end
    else begin
      if (!old_shift_clk & shift_clk) begin
        shift_out_hold <= SHIFT_OUT_HOLD;
        shift_out_ready <= 0;
      end
      else begin
        if (!shift_out_ready) begin
          if (shift_out_hold != 0) shift_out_hold <= shift_out_hold - 1;
          else begin
            matr_x <= matr_x + 1;
            shift_out[matr_x] <= matr_out > pwm_count;
            shift_out_ready <= matr_x == 3'b111;
          end
        end
      end
    end
  end

  assign matr_z = layer;
  always@(posedge prep_next or negedge reset) begin
    if (!reset) matr_y <= 0;
    else matr_y <= matr_y + 1;
  end

  // Rasterizer State Machine
  parameter STATE_READY = 0;
  reg [5:0] state = STATE_READY;

  assign ready = clk & (state == STATE_READY);

  always@(posedge clk or negedge reset) begin
    if (!reset)
      state <= STATE_READY;
    else begin
      case (state)

      STATE_READY: begin
        case (func)
        `RASTER_POINTS: begin
          matr_wa <= pos_in;
          matr_in <= value;
        end
        endcase
      end

      endcase
    end
  end

endmodule
