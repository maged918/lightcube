#!/usr/bin/env python2

############################### Description ##################################
# PyProcessing based utility to View LED Cube animations stored as matrices
# in a raw file
#
# ./animation_viewer.py path/to/animation_file.raw
#
# Mouse Controls
#   Left mouse drag to rotate
#   Right mouse drag or mouse scroll to zoom
# Keyboard Controls
#   Space Bar:  to start/stop playback
#   Left/Right: to fast forward or rewind during playback
#   Left/Right: to move one frame when playback is stopped
#   Home/End:   to skip to beginning/end of the animation
#   a:          toggle axis display
#
# File Format
#
#   +Z|  /+Y
#     | /
#     |/____+X
#
#   Each matrix is serialized going through X first, then Y, then Z.
#   For example the planes in demos/3dleds.com/TestXYZ_8x8x8_monochrome.raw:
#     YZ plane moving in positive X direction
#     XZ plane moving in positive Y direction
#     XY plane moving in positive Z direction
#
# Todo
#   OSD framerate, current frame, etc
#
##############################################################################
############################## Configurables #################################

cube_size = 8
led_size = 3
led_length = 40
distance = 900
axis_displacement = led_length

min_alpha = 20
max_alpha = 255

min_val = 150
max_val = 255

framerate = 25.0
playback_rate = 50.0
skip_rate = 100.0

bg = 0

##############################################################################

import sys
from pyprocessing import *

alpha_range = max_alpha - min_alpha
val_range = max_val - min_val

cur_frame = 0
input_file = open(sys.argv[1], 'rb')
data = bytearray(input_file.read()) #FIXME buffering for large files?

frame_size = cube_size * cube_size * cube_size
frames = len(data)/frame_size - 1

size(1024, 768)
frameRate(framerate)
lights()
sphereDetail(10)

upX = 0.0
upY = 0.0
upZ = 1.0

centerX = centerY = centerZ = (cube_size-1) * led_length/2.0
centerZ *= -1
eyeX = eyeY = eyeZ = 0.0
theta = 1.3
phi = -0.5

playback_count = 0
playback_div = framerate / playback_rate
skip_div = framerate / skip_rate
play = False
skip = 0

show_axis = True

def calc_camera():
  global theta, phi, eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upZ
  eyeX = centerX + cos(theta) * cos(phi) * distance
  eyeY = centerY - sin(theta) * cos(phi) * distance
  eyeZ = centerZ + sin(phi) * distance


def mouseDragged():
  global theta, phi, eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upZ, distance
  if mouse.button == LEFT:
    theta += (mouse.x - pmouse.x) * 0.01
    phi   -= (mouse.y - pmouse.y) * 0.01
    if phi > 1.57: # PI/2
      phi = 1.57
    if phi < -1.57:
      phi = -1.57
  elif mouse.button == RIGHT:
    distance += mouse.y - pmouse.y
  calc_camera()

# No access from pyprocessing, so must go directly to pyglet
@canvas.window.event
def on_mouse_scroll(x, y, scrollx, scrolly):
  global distance
  distance -= distance * 0.1 * scrolly
  calc_camera()

def keyPressed():
  global play, skip, cur_frame, show_axis
  if key.code == HOME: cur_frame = 0
  elif key.code == END: cur_frame = frames
  elif key.code == RIGHT:
    if play: skip = 1
    elif cur_frame < frames: cur_frame += 1
  elif key.code == LEFT:
    if play: skip = -1
    elif cur_frame > 0: cur_frame -= 1
  elif key.char == ' ':
    play = not play
    if cur_frame == frames: cur_frame = 0
  elif key.char == 'a':
    show_axis = not show_axis

def keyReleased():
  global play, skip
  if key.code == RIGHT or key.code == LEFT: skip = 0

calc_camera()

def draw():
  global cur_frame, playback_count, playback_div, skip_div, play, skip
  global eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upZ
  global bg, show_axis
  background(bg)

  camera(eyeX, eyeY, eyeZ,
         centerX, centerY, centerZ,
         upX, upY, upZ)

  if show_axis:
    pushMatrix()
    translate(-axis_displacement, -axis_displacement, axis_displacement)

    axis_length = cube_size*(led_length+4)
    fill(255)
    sphere(led_size * 2)
    stroke(255)
    line(0, 0, 0, axis_length, 0, 0);
    line(0, 0, 0, 0, axis_length, 0);
    line(0, 0, 0, 0, 0, -axis_length);
    noStroke()

    pushMatrix()
    fill(255, 0, 0)
    translate(axis_length, 0, 0)
    sphere(led_size * 2)
    popMatrix()

    pushMatrix()
    fill(0, 255, 0)
    translate(0, axis_length, 0)
    sphere(led_size * 2)
    popMatrix()


    pushMatrix()
    fill(0, 0, 255)
    translate(0, 0, -axis_length)
    sphere(led_size * 2)
    popMatrix()
    popMatrix()

  x = y = z = 0
  tr_x = tr_y = tr_z = 0
  for i in data[cur_frame*frame_size:]:
    val = i/255.0 * val_range + min_val
    fill(val, val, val, i/255.0*alpha_range + min_alpha)
    sphere(led_size)
    x += 1
    if x != cube_size:
      tr_x = led_length
      tr_y = tr_z = 0
    else:
      tr_x = (cube_size-1) * -led_length
      x = 0
      y += 1
      if y != cube_size:
        tr_y = led_length
        tr_z = 0
      else:
        tr_y = (cube_size-1) * -led_length
        y = 0
        z += 1
        if z != cube_size:
          tr_z = led_length
        else:
          break
    translate(tr_x, tr_y, -tr_z)

  if play:
    playback_count += 1
    if (skip != 0 and playback_count >= skip_div) or playback_count > playback_div:
      playback_count = 0
      if skip != 0: cur_frame += skip
      else: cur_frame += 1
      if cur_frame <= 0 or cur_frame >= frames:
        play = 0
        skip = 0
      if cur_frame < 0: cur_frame = 0
      if cur_frame > frames: cur_frame = frames

if __name__ == "__main__":
  run()

