/*############################# Description ##################################
# GLUT based utility to View LED Cube animations stored as matrices
# in a raw file
#
# g++ animation_viewer.cpp -o animation_viewer -lGL -lGLU -lglut
# ./animation_viewer path/to/animation_file.raw
#
# Mouse Controls
#   Left mouse drag to rotate
#   Right mouse drag or mouse scroll to zoom
# Keyboard Controls
#   Escape:     Exit
#   Space Bar:  Start/stop playback
#   Left/Right: Fast Forward or Rewind during playback; press again to disable.
#   Left/Right: Move one frame when playback is stopped
#   Home/End:   Skip to beginning/end of the animation
#   a:          Toggle axis display
#
# File Format
#
#   +Z|  /+Y
#     | /
#     |/____+X
#
#   Each matrix is serialized going through X first, then Y, then Z.
#   For example the planes in demos/3dleds.com/TestXYZ_8x8x8_monochrome.raw:
#     YZ plane moving in positive X direction
#     XZ plane moving in positive Y direction
#     XY plane moving in positive Z direction
#
# Todo
#   OSD framerate, current frame, etc
#
##############################################################################
############################## Configurables ###############################*/


#define CUBE_SIZE 8
#define LED_SIZE 3
#define LED_LENGTH 20
#define START_DISTANCE 230
#define SCROLL_SPEED 4
#define AXIS_DISPLACEMENT LED_LENGTH
#define AXIS_LENGTH (CUBE_SIZE*(LED_LENGTH+4))

#define SPHERE_DETAIL 15

#define MIN_ALPHA 20
#define MAX_ALPHA 255

#define MIN_VAL 150
#define MAX_VAL 255

#define FRAMERATE 25
#define SKIP_RATE 2

#define BG 10

/*##########################################################################*/

#include <iostream>
#include <fstream>

#include <GL/gl.h>
#include <GL/glut.h>
#include <math.h>

#define FRAME_SIZE (CUBE_SIZE*CUBE_SIZE*CUBE_SIZE)
#define ALPHA_RANGE (MAX_ALPHA - MIN_ALPHA)
#define VAL_RANGE (MAX_VAL - MIN_VAL)

#define draw_sphere(r) glutSolidSphere(r, SPHERE_DETAIL, SPHERE_DETAIL)

int cur_frame, frames;
unsigned char* data = NULL;

double distance;
double upX, upY, upZ;
double centerX, centerY, centerZ;
double eyeX, eyeY, eyeZ;
double phi, theta;

long last_time, cur_time;

int frame_interval;
int skip;
bool play, show_axis;

void recalculate_camera(void) {
  eyeX = centerX + cos(theta) * cos(phi) * distance;
  eyeY = centerY - sin(theta) * cos(phi) * distance;
  eyeZ = centerZ + sin(phi) * distance;
}

void quit(int status) {
  if (data != NULL) delete data;
  exit(status);
}

void init(char* filename) {
  unsigned int file_size;
  std::ifstream f_in(filename, std::ios::in | std::ios::binary | std::ios::ate);
  if (!f_in.is_open()) {
    std::cout << "Could not open \"" << filename << "'\n";
    quit(1);
  }
  file_size = f_in.tellg();
  f_in.seekg(0);
  data = new unsigned char[file_size];
  f_in.read((char*)data, file_size);
  f_in.close();
  cur_frame = 0;
  frames = file_size/FRAME_SIZE - 1;

  distance = START_DISTANCE;
  centerX = centerY = centerZ = (CUBE_SIZE-1) * LED_LENGTH/2.0;
  upX = upY = 0.0;
  upZ = 1.0;

  theta = 1.2;
  phi = 0.4;

  recalculate_camera();

  last_time = glutGet(GLUT_ELAPSED_TIME);

  frame_interval = 1000 / FRAMERATE;
  play = false;
  skip = 0;

  show_axis = true;

  glEnable(GL_LINE_SMOOTH);
  glEnable(GL_POLYGON_SMOOTH);
  glEnable(GL_MULTISAMPLE);
  glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
  glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glShadeModel(GL_SMOOTH);
  glClearColor(0.0f, 0.0f, 0.0f, 0.5f);
  glClearDepth(1.0f);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);
  glEnable(GL_COLOR_MATERIAL);
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void display(void) {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glClearColor(BG/255.0, BG/255.0, BG/255.0, 0);
  glLoadIdentity();

  gluLookAt(eyeX, eyeY, eyeZ,
            centerX, centerY, centerZ,
            upX, upY, upZ);

  if (show_axis) {
    glPushMatrix();
    glTranslated(-AXIS_DISPLACEMENT, -AXIS_DISPLACEMENT, -AXIS_DISPLACEMENT); //NEGZ

    glColor3ub(255, 255, 255);
    draw_sphere(LED_SIZE*2);
    glBegin(GL_LINES);
      glVertex3d(0, 0, 0); glVertex3d(AXIS_LENGTH, 0, 0);
    glEnd();
    glBegin(GL_LINES);
      glVertex3d(0, 0, 0); glVertex3d(0, AXIS_LENGTH, 0);
    glEnd();
    glBegin(GL_LINES);
      glVertex3d(0, 0, 0); glVertex3d(0, 0, AXIS_LENGTH);
    glEnd();

    glPushMatrix();
    glColor3ub(255, 0, 0);
    glTranslated(AXIS_LENGTH, 0, 0);
    draw_sphere(LED_SIZE*2);
    glPopMatrix();

    glPushMatrix();
    glColor3ub(0, 255, 0);
    glTranslated(0, AXIS_LENGTH, 0);
    draw_sphere(LED_SIZE*2);
    glPopMatrix();

    glPushMatrix();
    glColor3ub(0, 0, 255);
    glTranslated(0, 0, AXIS_LENGTH);
    draw_sphere(LED_SIZE*2);
    glPopMatrix();

    glPopMatrix();
  }

  int x, y, z, tr_x, tr_y, tr_z;
  x = y = z = 0;
  double val, alpha;
  int off = cur_frame * FRAME_SIZE;
  int frame_end = off + FRAME_SIZE;
  for (; off < frame_end; off++) {
    val = data[off]/255.0 * VAL_RANGE + MIN_VAL;
    alpha = data[off]/255.0 * ALPHA_RANGE + MIN_ALPHA;
    glColor4ub(val, val, val, alpha);
    draw_sphere(LED_SIZE);

    x += 1;
    if (x != CUBE_SIZE) {
      tr_x = LED_LENGTH;
      tr_y = tr_z = 0;
    } else {
      tr_x = (CUBE_SIZE-1) * -LED_LENGTH;
      x = 0;
      y += 1;
      if (y != CUBE_SIZE) {
        tr_y = LED_LENGTH;
        tr_z = 0;
      } else {
        tr_y = (CUBE_SIZE-1) * -LED_LENGTH;
        y = 0;
        z += 1;
        if (z != CUBE_SIZE) {
          tr_z = LED_LENGTH;
        } else {
          break;
        }
      }
    }

    glTranslatef(tr_x, tr_y, tr_z); // NEGZ
  }

  glutSwapBuffers();
}

void draw_frame_callback(int val) {
  if (play) {
    if (skip) cur_frame += skip * SKIP_RATE;
    else cur_frame += 1;
    if (cur_frame > frames) {
      play = skip = 0; cur_frame = frames;
    } else if (cur_frame < 0) {
      play = skip = 0; cur_frame = 0;
    }
  }
  glutPostRedisplay();
  glutTimerFunc(frame_interval, draw_frame_callback, 0);
}

void reshape(int w, int h) {
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  if (h == 0)
     gluPerspective(80, (float)w, 1.0, 5000.0);
  else
     gluPerspective(80, (float)w / (float)h, 1.0, 5000.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

int last_drag_x = 0, last_drag_y = 0, drag_button = 0;
bool dragging = false;
void mouse_click(int button, int state, int x, int y) {
  if (state == GLUT_DOWN && (button == 3 || button == 4)) {
    distance += button == 4 ? SCROLL_SPEED : -SCROLL_SPEED;
    if (distance < 0) distance = 0;
    recalculate_camera();
  } else {
    if (!dragging && state == GLUT_DOWN) {
        dragging = true;
        last_drag_x = x;
        last_drag_y = y;
        drag_button = button;
    } else if (state == GLUT_UP) {
      dragging = false;
    }
  }
}

void mouse_motion(int x, int y) {
  if (!dragging) return;
  if (drag_button == GLUT_LEFT_BUTTON) {
    theta += (x - last_drag_x) * 0.01;
    phi   += (y - last_drag_y) * 0.01;
    if (phi > 1.57) // PI/2
      phi = 1.57;
    else if (phi < -1.57)
      phi = -1.57;
  } else if (drag_button == GLUT_RIGHT_BUTTON) {
    distance += y - last_drag_y;
    if (distance < 0) distance = 0;
  }

  last_drag_x = x;
  last_drag_y = y;
  recalculate_camera();
}

void keyboard(unsigned char key, int x, int y) {
  switch (key) {
    case 27:
      quit(0);
      break;
    case ' ':
      play = !play;
      if (cur_frame == frames) cur_frame = 0;
      break;
    case 'a':
      show_axis = !show_axis;
      break;
  }
}

void special_keys(int key, int x, int y) {
  switch (key) {
    case GLUT_KEY_RIGHT:
      if (play) skip = skip != 1;
      else if (cur_frame < frames) cur_frame += 1;
      break;
    case GLUT_KEY_LEFT:
      if (play) skip = skip != -1 ? -1 : 0;
      else if (cur_frame > 0) cur_frame -= 1;
      break;
    case GLUT_KEY_HOME:
      cur_frame = 0;
      break;
    case GLUT_KEY_END:
      cur_frame = frames;
      break;
  }
}

int main (int argc, char** argv) {
  if (argc < 2) {
    std::cout << "Usage: " << argv[0] << " path/to/animation.raw\n";
    quit(1);
  }
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE | GLUT_MULTISAMPLE);
  glutInitWindowSize(1024, 768);
  glutCreateWindow("LED Cube Animation Viewer");
  init(argv[1]);
  //glutFullScreen();
  glutDisplayFunc(display);
  glutReshapeFunc(reshape);
  glutKeyboardFunc(keyboard);
  glutMouseFunc(mouse_click);
  glutMotionFunc(mouse_motion);
  glutSpecialFunc(special_keys);

  draw_frame_callback(0);

  glutMainLoop();
  quit(0);
  return 0;
}

