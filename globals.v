`define IS_POWER_OF_2_CUBE
`define CUBOID_LENGTH 8
`define CUBOID_WIDTH 8
`define CUBOID_HEIGHT 8
`define VOXEL_DEPTH 8

/*************************** End of Configurables ****************************/
/*****************************************************************************/

`define CUBOID_SIZE (`CUBOID_LENGTH*`CUBOID_WIDTH*`CUBOID_HEIGHT)

`define MATRIX_LAYER_SIZE (`CUBOID_LENGTH*`CUBOID_WIDTH)

`define MATRIX_ENTRIES `CUBOID_SIZE
`define LAYER_INDEX_WIDTH $clog2(`MATRIX_LAYER_SIZE)
`define MATRIX_X_WIDTH $clog2(`CUBOID_LENGTH)
`define MATRIX_Y_WIDTH $clog2(`CUBOID_WIDTH)
`define MATRIX_Z_WIDTH $clog2(`CUBOID_HEIGHT)

//`define MATRIX_INDEX(i) [`MATRIX_INDEX_BITS-1:0];

//`define X_INDEX(i) i[`MATRIX_INDEX_BITS-1:`MATRIX_INDEX_BITS-`MATRIX_X_BITS]
//`define Y_INDEX(i) i[`MATRIX_INDEX_BITS-`MATRIX_X_BITS-1:`MATRIX_INDEX_BITS-`MATRIX_X_BITS-`MATRIX_Y_BITS]
//`define Z_INDEX(i) i[`MATRIX_Z_BITS-1:0]

// Rasterizer Functions
`define RASTER_POINTS 1
`define RASTER_LINES  2

// Rasterizer Control

