// 512x8 frame memory

module frame_mem(input clk,
                 output reg [7:0] q, input [7:0] d,
                 input [8:0] write_addr, input [8:0] read_addr,
                 input write_en);

  reg [7:0] mem [511:0];

  always@(posedge clk) begin
    if (write_en) mem[write_addr] <= d;
    q <= mem[read_addr];
  end

endmodule
