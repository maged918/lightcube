module data_kb(clk, serial, hex);

input clk, serial;

output [7:0] hex;

reg counter, parity, byteDone;
reg [7:0] byte;

binary_map_hex rightDigit(byte[3:0],byteDone,hex[3:0]);
binary_map_hex leftDigit(byte[7:4],byteDone,hex[7:4]);

always@(negedge clk)begin
if(!counter & !serial)begin
	counter = counter + 1;
	byteDone=0;
end
else if(counter>0 & counter<9)begin 
	byte[counter-1] = serial;
	counter = counter + 1;
end
else if(counter == 9)begin
	counter = counter + 1;
	parity = serial;
end
if(counter == 10)begin
	counter = 0;
	if(serial)
		byteDone = 1;
end
end
endmodule
