#call str_to_ascii to give you verilog code for 2 words on lcd lines
def str_to_ascii(w1,w2):
    print '//Line 1'
    helper(w1,1)
    print '//Change Line'
    print 'LCD_CH_LINE:	LUT_DATA <= 9\'h0C0;'
    print '//Line 2'
    helper(w2,2)

def helper(w, line):
    w = ' ' + w
    a = ['0x%x' % (ord(c)+2**8) for c in w]
    space = '0x%x' % (ord(' ')+2**8)
    i = 0
    for hex in a:
        print ('LCD_LINE'+str(line)+'+'+str(i)+': LUT_DATA <= 9\'h'+hex+';')
        i+=1
        if(i>15):
            return
    while i<16:
        print ('LCD_LINE'+str(line)+'+'+str(i)+': LUT_DATA <= 9\'h'+space+';')
        i+=1
