module keyboard(clk50, reset, ps2_clk, serial, read_kb, right, left , up, down, planeUp, planeDown, space, esc, digit0, digit1);

inout ps2_clk, serial;

input reset, clk50, read_kb;

output right, left , up, down, planeUp, planeDown, space, esc;
output [6:0] digit0, digit1;

reg right, left , up, down, planeUp, planeDown, space, esc;

ps2_keyboard_interface data1(.clk(clk50), .ps2_clk(ps2_clk) , 
.ps2_data(serial), .reset(reset), 
.rx_scan_code(hex), .rx_read(read_kb) ,.digit0(digit0), .digit1(digit1));

always@(hex)begin
right = 0;
left = 0;
up = 0;
down = 0;
planeDown = 0;
planeUp = 0;
space = 0;
esc = 0;
case(hex)
8'h74 : right<=1;
8'h6B : left<=1;
8'h75 : up<=1;
8'h72 : down<=1;
8'h1C : planeUp<=1;
8'h1A : planeDown<=1;
8'h29 : space<=1;
8'h76 : esc<=1;
endcase
end
endmodule