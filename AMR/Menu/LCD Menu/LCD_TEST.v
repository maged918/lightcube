module	LCD_TEST (	//	Host Side
					iCLK,iRST_N, 
					//keyboard
					/*ps2_clk, ps2_data, read_kb,
					rx_extended,
					rx_released,
					rx_shift_key_on,
					rx_scan_code,
					rx_ascii,
					rx_data_ready,       // rx_read_o
					*/
					up_key,
					down_key,
					enter_key,
					//rx_scan_code,
					//	LCD Side
					LCD_DATA,LCD_RW,LCD_EN,LCD_RS,
					L3,L4,L5
					// 7 Segment Side
					//HEX0, HEX1
					);
//	Host Side
input	iCLK,iRST_N;
//	LCD Side
output	[7:0]	LCD_DATA;
output			LCD_RW,LCD_EN,LCD_RS;
output L3,L4,L5;
reg L3,L4,L5;
//	Internal Wires/Registers
reg	[5:0]	LUT_INDEX;
reg	[8:0]	LUT_DATA;
reg	[5:0]	mLCD_ST;
reg	[17:0]	mDLY;
reg			mLCD_Start;
reg	[7:0]	mLCD_DATA;
reg			mLCD_RS;
wire		mLCD_Done;

parameter	LCD_INTIAL	=	0;
parameter	LCD_LINE1	=	5;
parameter	LCD_CH_LINE	=	LCD_LINE1+16;
parameter	LCD_LINE2	=	LCD_LINE1+16+1;
parameter	LUT_SIZE	=	LCD_LINE1+32+1;

reg resetDone, upPressed, downPressed, enterPressed;

/*
output rx_extended;
output rx_released;
output rx_shift_key_on;
output [7:0] rx_scan_code;
output [7:0] rx_ascii;
output rx_data_ready;

input read_kb;
inout ps2_clk,ps2_data;
wire up_key, down_key, enter_key;
ps2_keyboard_interface kb1(
  iClK,
  iRsT_N,
  ps2_clk,
  ps2_data,
  rx_extended,
  rx_released,
  rx_shift_key_on,
  rx_scan_code,
  rx_ascii,
  rx_data_ready,       // rx_read_o
  read_kb,
  
  up_key,
  down_key,
  enter_key
);
*/
input up_key,down_key,enter_key;

always@(posedge iCLK or posedge up_key or posedge down_key or posedge enter_key or negedge iRST_N)
begin
	if(up_key)begin
	   upPressed <= 1;
		downPressed <= 0;
		enterPressed <= 0;
		L3 <= 1;
		L4 <= 0;
		L5 <= 0;
	end
	else if(down_key)begin
	   upPressed <= 0;
		downPressed <= 1;
		enterPressed <= 0;
		L3 <= 0;
		L4 <= 1;
		L5 <= 0;
	end
	else if(enter_key)begin
		upPressed <= 0;
		downPressed <= 0;
		enterPressed <= 1;
		L3 <= 0;
		L4 <= 0;
		L5 <= 1;
	end
	
	if(!iRST_N || up_key || down_key || enter_key)
	begin
		LUT_INDEX	<=	0;
		mLCD_ST		<=	0;
		mDLY		<=	0;
		mLCD_Start	<=	0;
		mLCD_DATA	<=	0;
		mLCD_RS		<=	0;
		resetDone <= 1;
	end
	else
	begin
		resetDone <= 0;
		if(LUT_INDEX<LUT_SIZE)
		begin
			case(mLCD_ST)
			0:	begin
					mLCD_DATA	<=	LUT_DATA[7:0];
					mLCD_RS		<=	LUT_DATA[8];
					mLCD_Start	<=	1;
					mLCD_ST		<=	1;
				end
			1:	begin
					if(mLCD_Done)
					begin
						mLCD_Start	<=	0;
						mLCD_ST		<=	2;					
					end
				end
			2:	begin
					if(mDLY<18'h3FFFE)
					mDLY	<=	mDLY+1;
					else
					begin
						mDLY	<=	0;
						mLCD_ST	<=	3;
					end
				end
			3:	begin
					LUT_INDEX	<=	LUT_INDEX+1;
					mLCD_ST	<=	0;
				end
			endcase
		end
	end
end

reg [2:0] menu_state;
parameter views_num = 3;

always@(negedge resetDone)//posedge enter_key or 
begin
	if(upPressed)begin
	   if (menu_state > 0)
			menu_state <= menu_state - 1;
      else
		   menu_state <= views_num - 1;
	end
	else if(downPressed)begin
	   if (menu_state < views_num - 1)
			menu_state <= menu_state + 1;
      else
		   menu_state <= 0;
	end
	else if(enterPressed)begin
		menu_state <= 3;
	end
end

always
begin
	case(menu_state)
		//First View
		0:begin
			case(LUT_INDEX)
			//	Initial
			LCD_INTIAL+0:	LUT_DATA	<=	9'h038;
			LCD_INTIAL+1:	LUT_DATA	<=	9'h00C;
			LCD_INTIAL+2:	LUT_DATA	<=	9'h001;
			LCD_INTIAL+3:	LUT_DATA	<=	9'h006;
			LCD_INTIAL+4:	LUT_DATA	<=	9'h080;
			
			//Line 1
			LCD_LINE1+0: LUT_DATA <= 9'h0x120;
			LCD_LINE1+1: LUT_DATA <= 9'h0x156;
			LCD_LINE1+2: LUT_DATA <= 9'h0x169;
			LCD_LINE1+3: LUT_DATA <= 9'h0x173;
			LCD_LINE1+4: LUT_DATA <= 9'h0x175;
			LCD_LINE1+5: LUT_DATA <= 9'h0x161;
			LCD_LINE1+6: LUT_DATA <= 9'h0x16c;
			LCD_LINE1+7: LUT_DATA <= 9'h0x120;
			LCD_LINE1+8: LUT_DATA <= 9'h0x131;
			LCD_LINE1+9: LUT_DATA <= 9'h0x120;
			LCD_LINE1+10: LUT_DATA <= 9'h0x120;
			LCD_LINE1+11: LUT_DATA <= 9'h0x120;
			LCD_LINE1+12: LUT_DATA <= 9'h0x120;
			LCD_LINE1+13: LUT_DATA <= 9'h0x120;
			LCD_LINE1+14: LUT_DATA <= 9'h0x120;
			LCD_LINE1+15: LUT_DATA <= 9'h0x120;
			//Change Line
			LCD_CH_LINE:	LUT_DATA <= 9'h0C0;
			//Line 2
			LCD_LINE2+0: LUT_DATA <= 9'h0x120;
			LCD_LINE2+1: LUT_DATA <= 9'h0x146;
			LCD_LINE2+2: LUT_DATA <= 9'h0x169;
			LCD_LINE2+3: LUT_DATA <= 9'h0x172;
			LCD_LINE2+4: LUT_DATA <= 9'h0x173;
			LCD_LINE2+5: LUT_DATA <= 9'h0x174;
			LCD_LINE2+6: LUT_DATA <= 9'h0x120;
			LCD_LINE2+7: LUT_DATA <= 9'h0x147;
			LCD_LINE2+8: LUT_DATA <= 9'h0x172;
			LCD_LINE2+9: LUT_DATA <= 9'h0x16f;
			LCD_LINE2+10: LUT_DATA <= 9'h0x175;
			LCD_LINE2+11: LUT_DATA <= 9'h0x170;
			LCD_LINE2+12: LUT_DATA <= 9'h0x120;
			LCD_LINE2+13: LUT_DATA <= 9'h0x120;
			LCD_LINE2+14: LUT_DATA <= 9'h0x120;
			LCD_LINE2+15: LUT_DATA <= 9'h0x120;
			default:		LUT_DATA	<=	9'h120;
			endcase
		end		
		// Second View
		1:begin
			case(LUT_INDEX)
			//	Initial
			LCD_INTIAL+0:	LUT_DATA	<=	9'h038;
			LCD_INTIAL+1:	LUT_DATA	<=	9'h00C;
			LCD_INTIAL+2:	LUT_DATA	<=	9'h001;
			LCD_INTIAL+3:	LUT_DATA	<=	9'h006;
			LCD_INTIAL+4:	LUT_DATA	<=	9'h080;
			
			//Line 1
			LCD_LINE1+0: LUT_DATA <= 9'h0x120;
			LCD_LINE1+1: LUT_DATA <= 9'h0x156;
			LCD_LINE1+2: LUT_DATA <= 9'h0x169;
			LCD_LINE1+3: LUT_DATA <= 9'h0x173;
			LCD_LINE1+4: LUT_DATA <= 9'h0x175;
			LCD_LINE1+5: LUT_DATA <= 9'h0x161;
			LCD_LINE1+6: LUT_DATA <= 9'h0x16c;
			LCD_LINE1+7: LUT_DATA <= 9'h0x120;
			LCD_LINE1+8: LUT_DATA <= 9'h0x132;
			LCD_LINE1+9: LUT_DATA <= 9'h0x120;
			LCD_LINE1+10: LUT_DATA <= 9'h0x120;
			LCD_LINE1+11: LUT_DATA <= 9'h0x120;
			LCD_LINE1+12: LUT_DATA <= 9'h0x120;
			LCD_LINE1+13: LUT_DATA <= 9'h0x120;
			LCD_LINE1+14: LUT_DATA <= 9'h0x120;
			LCD_LINE1+15: LUT_DATA <= 9'h0x120;
			//Change Line
			LCD_CH_LINE:	LUT_DATA <= 9'h0C0;
			//Line 2
			LCD_LINE2+0: LUT_DATA <= 9'h0x120;
			LCD_LINE2+1: LUT_DATA <= 9'h0x153;
			LCD_LINE2+2: LUT_DATA <= 9'h0x165;
			LCD_LINE2+3: LUT_DATA <= 9'h0x163;
			LCD_LINE2+4: LUT_DATA <= 9'h0x16f;
			LCD_LINE2+5: LUT_DATA <= 9'h0x16e;
			LCD_LINE2+6: LUT_DATA <= 9'h0x164;
			LCD_LINE2+7: LUT_DATA <= 9'h0x120;
			LCD_LINE2+8: LUT_DATA <= 9'h0x147;
			LCD_LINE2+9: LUT_DATA <= 9'h0x172;
			LCD_LINE2+10: LUT_DATA <= 9'h0x16f;
			LCD_LINE2+11: LUT_DATA <= 9'h0x175;
			LCD_LINE2+12: LUT_DATA <= 9'h0x170;
			LCD_LINE2+13: LUT_DATA <= 9'h0x120;
			LCD_LINE2+14: LUT_DATA <= 9'h0x120;
			LCD_LINE2+15: LUT_DATA <= 9'h0x120;
						
			default:		LUT_DATA	<=	9'h120;
			endcase
		end
		// Third View
		2:begin
			case(LUT_INDEX)
			//	Initial
			LCD_INTIAL+0:	LUT_DATA	<=	9'h038;
			LCD_INTIAL+1:	LUT_DATA	<=	9'h00C;
			LCD_INTIAL+2:	LUT_DATA	<=	9'h001;
			LCD_INTIAL+3:	LUT_DATA	<=	9'h006;
			LCD_INTIAL+4:	LUT_DATA	<=	9'h080;
			
			//Line 1
			LCD_LINE1+0: LUT_DATA <= 9'h0x120;
			LCD_LINE1+1: LUT_DATA <= 9'h0x133;
			LCD_LINE1+2: LUT_DATA <= 9'h0x144;
			LCD_LINE1+3: LUT_DATA <= 9'h0x120;
			LCD_LINE1+4: LUT_DATA <= 9'h0x153;
			LCD_LINE1+5: LUT_DATA <= 9'h0x16e;
			LCD_LINE1+6: LUT_DATA <= 9'h0x161;
			LCD_LINE1+7: LUT_DATA <= 9'h0x16b;
			LCD_LINE1+8: LUT_DATA <= 9'h0x165;
			LCD_LINE1+9: LUT_DATA <= 9'h0x120;
			LCD_LINE1+10: LUT_DATA <= 9'h0x120;
			LCD_LINE1+11: LUT_DATA <= 9'h0x120;
			LCD_LINE1+12: LUT_DATA <= 9'h0x120;
			LCD_LINE1+13: LUT_DATA <= 9'h0x120;
			LCD_LINE1+14: LUT_DATA <= 9'h0x120;
			LCD_LINE1+15: LUT_DATA <= 9'h0x120;
			//Change Line
			LCD_CH_LINE:	LUT_DATA <= 9'h0C0;
			//Line 2
			LCD_LINE2+0: LUT_DATA <= 9'h0x120;
			LCD_LINE2+1: LUT_DATA <= 9'h0x149;
			LCD_LINE2+2: LUT_DATA <= 9'h0x16e;
			LCD_LINE2+3: LUT_DATA <= 9'h0x174;
			LCD_LINE2+4: LUT_DATA <= 9'h0x172;
			LCD_LINE2+5: LUT_DATA <= 9'h0x161;
			LCD_LINE2+6: LUT_DATA <= 9'h0x163;
			LCD_LINE2+7: LUT_DATA <= 9'h0x174;
			LCD_LINE2+8: LUT_DATA <= 9'h0x176;
			LCD_LINE2+9: LUT_DATA <= 9'h0x120;
			LCD_LINE2+10: LUT_DATA <= 9'h0x147;
			LCD_LINE2+11: LUT_DATA <= 9'h0x161;
			LCD_LINE2+12: LUT_DATA <= 9'h0x16d;
			LCD_LINE2+13: LUT_DATA <= 9'h0x165;
			LCD_LINE2+14: LUT_DATA <= 9'h0x120;
			LCD_LINE2+15: LUT_DATA <= 9'h0x120;
			
			default:		LUT_DATA	<=	9'h120;
			endcase
		end
			
			3:begin
			case(LUT_INDEX)
			//	Initial
			LCD_INTIAL+0:	LUT_DATA	<=	9'h038;
			LCD_INTIAL+1:	LUT_DATA	<=	9'h00C;
			LCD_INTIAL+2:	LUT_DATA	<=	9'h001;
			LCD_INTIAL+3:	LUT_DATA	<=	9'h006;
			LCD_INTIAL+4:	LUT_DATA	<=	9'h080;
			
			//Line 1
			LCD_LINE1+0: LUT_DATA <= 9'h0x120;
			LCD_LINE1+1: LUT_DATA <= 9'h0x14c;
			LCD_LINE1+2: LUT_DATA <= 9'h0x16f;
			LCD_LINE1+3: LUT_DATA <= 9'h0x161;
			LCD_LINE1+4: LUT_DATA <= 9'h0x164;
			LCD_LINE1+5: LUT_DATA <= 9'h0x169;
			LCD_LINE1+6: LUT_DATA <= 9'h0x16e;
			LCD_LINE1+7: LUT_DATA <= 9'h0x167;
			LCD_LINE1+8: LUT_DATA <= 9'h0x12e;
			LCD_LINE1+9: LUT_DATA <= 9'h0x12e;
			LCD_LINE1+10: LUT_DATA <= 9'h0x120;
			LCD_LINE1+11: LUT_DATA <= 9'h0x120;
			LCD_LINE1+12: LUT_DATA <= 9'h0x120;
			LCD_LINE1+13: LUT_DATA <= 9'h0x120;
			LCD_LINE1+14: LUT_DATA <= 9'h0x120;
			LCD_LINE1+15: LUT_DATA <= 9'h0x120;
			//Change Line
			LCD_CH_LINE:	LUT_DATA <= 9'h0C0;
			//Line 2
			LCD_LINE2+0: LUT_DATA <= 9'h0x120;
			LCD_LINE2+1: LUT_DATA <= 9'h0x120;
			LCD_LINE2+2: LUT_DATA <= 9'h0x120;
			LCD_LINE2+3: LUT_DATA <= 9'h0x120;
			LCD_LINE2+4: LUT_DATA <= 9'h0x120;
			LCD_LINE2+5: LUT_DATA <= 9'h0x120;
			LCD_LINE2+6: LUT_DATA <= 9'h0x120;
			LCD_LINE2+7: LUT_DATA <= 9'h0x120;
			LCD_LINE2+8: LUT_DATA <= 9'h0x120;
			LCD_LINE2+9: LUT_DATA <= 9'h0x120;
			LCD_LINE2+10: LUT_DATA <= 9'h0x120;
			LCD_LINE2+11: LUT_DATA <= 9'h0x120;
			LCD_LINE2+12: LUT_DATA <= 9'h0x120;
			LCD_LINE2+13: LUT_DATA <= 9'h0x120;
			LCD_LINE2+14: LUT_DATA <= 9'h0x120;
			LCD_LINE2+15: LUT_DATA <= 9'h0x120;
			
			default:		LUT_DATA	<=	9'h120;
			endcase
		end
	endcase
end

LCD_Controller u0(
					//	Host Side
					.iDATA(mLCD_DATA),
					.iRS(mLCD_RS),
					.iStart(mLCD_Start),
					.oDone(mLCD_Done),
					.iCLK(iCLK),
					.iRST_N(iRST_N),
					//	LCD Interface
					.LCD_DATA(LCD_DATA),
					.LCD_RW(LCD_RW),
					.LCD_EN(LCD_EN),
					.LCD_RS(LCD_RS));

endmodule