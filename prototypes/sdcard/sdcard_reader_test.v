module sdcard_reader_test(
  input CLOCK_50,
  input DI, output nCS, output SD_CLK, output DO,
  input [3:0] KEY, input [17:0] SW, output [17:0] LEDR,
  output [6:0] HEX0, output [6:0] HEX1, output [6:0] HEX2,
  output [6:0] HEX3, output [6:0] HEX4, output [6:0] HEX5,
  output [6:0] HEX6, output [6:0] HEX7, inout [35:0] GPIO);

  wire [4:0] rastr_func;
  wire [8:0] rastr_pos;
  wire [7:0] rastr_val;
  wire [33:0] debug;

  wire ready;
  assign reset = KEY[0];

  reg [32:0] frclk_div = 0;
  reg frclk = 0;
  reg [9:0] frame_num = 0;
  parameter BASE_CLK = 50_000_000;
  parameter FPS = 40;
  wire [31:0] FPS_DIV = (BASE_CLK) / FPS - 1;

  always@(posedge CLOCK_50) begin
    if (frclk_div >= FPS_DIV) begin
      frclk <= !frclk;
      frclk_div <= 0;
    end
    else frclk_div <= frclk_div + 1;
  end

  always@(posedge frclk or negedge reset) begin
    if (!reset) frame_num <= 0;
    else frame_num <= frame_num + 1;
  end

  assign GPIO[31] = 1;
  assign GPIO[3] = 0;
  assign GPIO[1] = 1;

  wire [7:0] shift_b, layer_out;
  assign {GPIO[25], GPIO[23], GPIO[21], GPIO[19],
          GPIO[17], GPIO[15], GPIO[13], GPIO[11]} = shift_b[7:0];
  assign {GPIO[24], GPIO[22], GPIO[20], GPIO[18],
          GPIO[16], GPIO[14], GPIO[12], GPIO[10]} = layer_out[7:0];

  sdcard_reader r1(CLOCK_50,
                   DI, nCS, SD_CLK, DO,
                   frame_num, reset, debug,
                   rastr_func, rastr_pos, rastr_val);

  rasterizer rastr(CLOCK_50, ready, frclk,
                    0, rastr_func, 0,
                    rastr_pos, rastr_val,
                    GPIO[33], GPIO[35],
                    shift_b, layer_out,
                    reset);

  assign LEDR = debug[33:32];

  seven_segment_decoder s0(debug[3:0],   HEX0);
  seven_segment_decoder s1(debug[7:4],   HEX1);
  seven_segment_decoder s2(debug[11:8],  HEX2);
  seven_segment_decoder s3(debug[15:12], HEX3);
  seven_segment_decoder s4(debug[19:16], HEX4);
  seven_segment_decoder s5(debug[23:20], HEX5);
  seven_segment_decoder s6(debug[27:24], HEX6);
  seven_segment_decoder s7(debug[31:28], HEX7);

endmodule
