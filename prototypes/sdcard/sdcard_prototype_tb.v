`timescale 1ns/1ns

module sdcard_prototype_tb();
  reg clk50 = 0;
  always #10 clk50 = !clk50;
  reg [3:0] key;

  reg [7:0] to_send = 8'b01001111;
  reg [2:0] to_send_ind = 0;
  wire sd_clk, nCS;

  sdcard_prototype sdcpt(.CLOCK_50(clk50), .KEY(key), .DI(di), .SD_CLK(sd_clk), .nCS(nCS));

  assign di = to_send[7];

  always@(negedge sd_clk or posedge nCS) begin
    if (nCS) to_send <= 8'b01001111;
    else to_send <= {to_send[6:0], to_send[7]};
  end

  initial begin
    $dumpfile("sdcard_prototype.lxt");
    $dumpvars(0, sdcard_prototype_tb);
    key[0] = 1;
    #1 key[0] = 0;
    #1 key[0] = 1;
    #999999 $stop();
  end
endmodule
