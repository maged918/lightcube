//`define SIMULATE 1

module sdcard_prototype (input CLOCK_50,
                         input DI, output nCS, output SD_CLK, output DO, input SD_WP_N,
                         input [3:0] KEY, output reg [17:0] LEDR,
                         output [6:0] HEX0, output [6:0] HEX1, output [6:0] HEX2,
                         output [6:0] HEX3, output [6:0] HEX4, output [6:0] HEX5,
                         output [6:0] HEX6, output [6:0] HEX7);

`ifdef SIMULATE
  parameter SPI_FREQ = 25_000_000;
  parameter INIT_DELAY = 16;
  parameter RESPONSE_TIMEOUT = 8;
`else
  parameter SPI_FREQ = 4_000;
  parameter INIT_DELAY = 1024;
  parameter RESPONSE_TIMEOUT = 100;
`endif

  wire [31:0] spi_freq = SPI_FREQ;
  wire spi_next_byte, spi_start;
  wire [7:0] spi_byte_incoming;
  reg [7:0] spi_byte_outgoing;

  spi_master m(CLOCK_50, spi_freq, spi_start, spi_next_byte,
               spi_byte_incoming, spi_byte_outgoing,
               SD_CLK, DO, DI, nCS);

  reg [$clog2(INIT_DELAY):0] init_delay = 0;
  reg [49:0] init_lut;
  reg [9:0] init_lut_index = 0;

  initial begin
    init_lut_index = 0;
  end

  assign reset = KEY[0];

// DEBUGING
  reg [31:0] debug;
  seven_segment_decoder s0(debug[3:0],   HEX0);
  seven_segment_decoder s1(debug[7:4],   HEX1);
  seven_segment_decoder s2(debug[11:8],  HEX2);
  seven_segment_decoder s3(debug[15:12], HEX3);
  seven_segment_decoder s4(debug[19:16], HEX4);
  seven_segment_decoder s5(debug[23:20], HEX5);
  seven_segment_decoder s6(debug[27:24], HEX6);
  seven_segment_decoder s7(debug[31:28], HEX7);


  assign spi_start = init_delay > INIT_DELAY;
  always@(negedge SD_CLK or negedge reset) begin
    if (!reset)
      init_delay <= 0;
    else
      if (init_delay <= INIT_DELAY) init_delay <= init_delay + 1;
  end

  parameter CMD_SEND_BYTE_1 = 1;
  parameter CMD_SEND_BYTE_2 = 2;
  parameter CMD_SEND_BYTE_3 = 3;
  parameter CMD_SEND_BYTE_4 = 4;
  parameter CMD_SEND_BYTE_5 = 5;
  parameter CMD_SEND_BYTE_6 = 6;

  parameter CMD_WAIT_RESP    = 7;
  parameter CMD_NO_WAIT_RESP = 7;
  parameter CMD_WAIT_RESP_R1 = 8;
  parameter CMD_WAIT_RESP_R2 = 9;
  parameter CMD_WAIT_RESP_R3 = 10;

  reg [9:0] cmd_comm_state;
  reg [9:0] resp_timeout;

  always@(posedge spi_next_byte or negedge reset) begin
    if (!reset) begin
      spi_byte_outgoing <= 8'hFF;
      cmd_comm_state <= CMD_SEND_BYTE_1;
      resp_timeout <= 0;
      debug <= 0;
      init_lut_index <= 0;
    end
    else begin
      case (cmd_comm_state)
      CMD_SEND_BYTE_1: begin
        debug[23:16] <= init_lut[47:40];
        spi_byte_outgoing <= init_lut[47:40];
        cmd_comm_state <= cmd_comm_state + 1;
      end
      CMD_SEND_BYTE_2: begin
        spi_byte_outgoing <= init_lut[39:32];
        cmd_comm_state <= cmd_comm_state + 1;
      end
      CMD_SEND_BYTE_3: begin
        spi_byte_outgoing <= init_lut[31:24];
        cmd_comm_state <= cmd_comm_state + 1;
      end
      CMD_SEND_BYTE_4: begin
        spi_byte_outgoing <= init_lut[23:16];
        cmd_comm_state <= cmd_comm_state + 1;
      end
      CMD_SEND_BYTE_5: begin
        spi_byte_outgoing <= init_lut[15:8];
        cmd_comm_state <= cmd_comm_state + 1;
      end
      CMD_SEND_BYTE_6: begin
        spi_byte_outgoing <= init_lut[7:0];
        cmd_comm_state <= CMD_WAIT_RESP + init_lut[49:48]; // Which response type to wait for
        resp_timeout <= RESPONSE_TIMEOUT;
      end
      CMD_NO_WAIT_RESP: begin
        spi_byte_outgoing <= 8'hFF;
        init_lut_index <= init_lut_index + 1;
        cmd_comm_state <= CMD_SEND_BYTE_1;
      end
      CMD_WAIT_RESP_R1: begin
        spi_byte_outgoing <= 8'hFF;
        if (resp_timeout != 0) begin
          resp_timeout <= resp_timeout - 1;
          if (spi_byte_incoming[7] == 0) begin // we got a response, next command please!
            debug[27:24] = debug[27:24] + 1;
            debug[15:0] <= {debug[7:0], spi_byte_incoming};
            init_lut_index <= init_lut_index + 1;
            cmd_comm_state <= CMD_SEND_BYTE_1;
          end
        end
        else begin // timed out, resend command
          debug[31:28] <= debug[31:28] + 1;
          cmd_comm_state <= CMD_SEND_BYTE_1;
        end
      end
      default: LEDR <= LEDR+1;
      endcase
    end
  end

  // Initialization Look Up Table
  // {2'b RESP_TYPE, 48'b CMD}
  // RESP_TYPE 0 means no wait
  parameter SPI_LINE_IDLE = {2'b00, 48'hFF_FF_FF_FF_FF_FF};
  always@(init_lut_index) begin
    case(init_lut_index)
    // CMD0
    0: init_lut <= SPI_LINE_IDLE;
    1: init_lut <= {2'b01, 2'b01, 6'd0, 40'h95};
	 2: init_lut <= {2'b01, 2'b01, 6'd17, 40'h95};
	 //2: init_lut <= {2'b01, 2'b01, 6'd1, 40'h95};
	 //3: init_lut <= {2'b01, 2'b01, 6'd55, 40'h95};
    //4: init_lut <= {2'b01, 2'b01, 6'd41, 40'h95};
    // CMD
    default: init_lut <= SPI_LINE_IDLE;
    endcase
  end

endmodule
