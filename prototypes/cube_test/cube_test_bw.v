module cube_test_bw(input clk_50, input next, input prev, input change,
                    input reset, output reg shift_clk, output shift_n_mr,
                    output shift_a, output reg [7:0] shift_b, output latch_clk,
                    output latch_n_en, output [7:0] layer_out,
                    output reg [2:0] plane_pos, output reg [1:0] plane);

  assign layer_out[7:0] = 1;
  assign shift_clk = clk_50;
  assign latch_clk = !shift_clk;
  assign shift_a = 1'b1;
  assign shift_n_mr = 1'b1;
  assign latch_n_en = 1'b0;

  //assign shift_b = 8'FF;
  always@(posedge shift_clk) begin
    shift_b = ~shift_b;
   end

endmodule
