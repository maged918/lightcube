module cube_test(input clk_50, input next, input prev, input change, input reset,
                 output shift_clk, output shift_n_mr, output shift_a, output reg [7:0] shift_b,
                 output latch_clk, output latch_n_en, output [7:0] layer_out,
                 output reg [2:0] plane_pos, output reg [1:0] plane);

  assign shift_clk = clk_50;
  reg [2:0] shift_cnt = 3'h7;
  reg [2:0] layer = 3'h7;

  assign shift_a = 1'b1;
  assign shift_n_mr = 1'b1;
  assign latch_n_en = 1'b0;

  initial begin
    plane = 0;
    plane_pos = 2;
    shift_cnt = 7;
    shift_b = 0;
    latch_clk = 0;
    layer = 7;
  end

  always@(posedge shift_clk) shift_cnt <= shift_cnt + 1;
  assign latch_clk = (shift_cnt == 0) & (!shift_clk);
  always@(posedge latch_clk) layer <= layer + 1;
  assign layer_out = 8'h1 << layer;

  // prepare the next bits to shift out
  always@(posedge clk_50) begin
    case (plane)
    1: begin // YZ
      shift_b <= (8'h80 >> plane_pos);
    end

    1: begin // XZ
      shift_b <= ((7 - shift_cnt) == plane_pos) ? 8'hFF : 8'h00;
    end

    2: begin // XY
      shift_b <= (layer == plane_pos) ? 8'hFF : 8'h00;
    end
    endcase
  end

  always@(negedge next or negedge prev) begin
     if (!next) plane_pos <= plane_pos + 1;
     else if (!prev) plane_pos <= plane_pos - 1;
  end

  always@(negedge change) plane <= (plane == 2) ? 0 : plane + 1;

endmodule
