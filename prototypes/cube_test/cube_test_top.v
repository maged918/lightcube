module cube_test_top(input CLOCK_50, input [17:0] SW, input [3:0] KEY,
                     output [35:0] GPIO, output [17:0] LEDR);
  wire [7:0] shift_b;
  wire [7:0] layer_out;
  wire [1:0] plane;
  wire [2:0] plane_pos;

  wire [17:0] FREQ = SW[17:0];
  parameter BASE_CLK = 50000000;
  wire [32:0] CLK_DIV = (BASE_CLK/2) / FREQ;
  reg [32:0] clk_div;
  reg clk;

  always@(posedge CLOCK_50) begin
    if (clk_div >= CLK_DIV) begin
      clk <= !clk;
      clk_div <= 0;
    end
     else clk_div <= clk_div + 1;
  end

  // cube_test: Key controlled planes
  // cube_test_bw: draw 1010101... pattern on top layer
  cube_test ct(.clk_50(clk), .next(KEY[2]), .prev(KEY[3]), .change(KEY[1]),
               .reset(1), .shift_clk(GPIO[33]), .shift_n_mr(GPIO[1]),
               .shift_a(GPIO[31]), .shift_b(shift_b[7:0]), .latch_clk(GPIO[35]),
               .latch_n_en(GPIO[3]), .layer_out(layer_out[7:0]), .plane(plane),
               .plane_pos(plane_pos));

  assign {GPIO[25], GPIO[23], GPIO[21], GPIO[19],
          GPIO[17], GPIO[15], GPIO[13], GPIO[11]} = shift_b[7:0];
  assign {GPIO[24], GPIO[22], GPIO[20], GPIO[18],
          GPIO[16], GPIO[14], GPIO[12], GPIO[10]} = layer_out[7:0];

  assign LEDR[7:0] = shift_b[7:0];
  assign LEDR[17:16] = plane[1:0];
  assign LEDR[14:12] = plane_pos[2:0];

endmodule
