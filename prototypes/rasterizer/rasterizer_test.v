// Test the rasterizer by drawing a point with co-ordinates taken from the switches
module rasterizer_test(input CLOCK_50,
                       input [17:0] SW, input [3:0] KEY,
                       input [35:0] GPIO);

  assign reset = KEY[0];

  assign GPIO[31] = 1;
  assign GPIO[3] = 0;
  assign GPIO[1] = 1;

  wire [7:0] shift_b, layer_out;
  assign {GPIO[25], GPIO[23], GPIO[21], GPIO[19],
          GPIO[17], GPIO[15], GPIO[13], GPIO[11]} = shift_b[7:0];
  assign {GPIO[24], GPIO[22], GPIO[20], GPIO[18],
          GPIO[16], GPIO[14], GPIO[12], GPIO[10]} = layer_out[7:0];

  reg [32:0] frclk_div = 0;
  reg frclk = 0;
  reg [9:0] frame_num = 0;
  parameter BASE_CLK = 50_000_000;
  parameter FPS = 25;
  wire [31:0] FPS_DIV = (BASE_CLK) / FPS - 1;

  always@(posedge CLOCK_50) begin
    if (frclk_div >= FPS_DIV) begin
      frclk <= !frclk;
      frclk_div <= 0;
    end
    else frclk_div <= frclk_div + 1;
  end

  wire [4:0] rastr_func;
  wire [8:0] rastr_pos;
  wire [7:0] rastr_val;

  assign rastr_pos = SW[8:0];
  assign rastr_func = 1;
  assign rastr_val = 255;

  rasterizer rastr(CLOCK_50, ready, frclk,
                   0, rastr_func, 0,
                   rastr_pos, rastr_val,
                   GPIO[33], GPIO[35],
                   shift_b, layer_out,
                   reset);
endmodule

