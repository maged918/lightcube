`include "globals.v"

module rasterizer(input clk, output ready, input frclk,
                  input [4:0] ctrl_flags, input [4:0] func, input [4:0] func_flags,
                  input [8:0] pos_in, input [7:0] value,
                  output reg shift_clk = 0, output reg latch_clk = 0,
                  output reg [7:0] shift_out = 8'h0, output reg [7:0] layer_out = 8'h40,
                  input reset);

  reg ready_sig = 0;

  reg matr_choose = 0;
  always@(posedge frclk) matr_choose <= !matr_choose;

  reg [8:0] matr_wa = 0, matr_ra = 0;
  reg [7:0] matr_in;
  wire [7:0] matr_out;
  wire matr_we = 1;

  // matrix memory
  // operated with inverted clock to make it negedge triggered
  double_buffer dbmat(!clk,
                      matr_in, matr_out,
                      matr_wa, {matr_ra[2:0], matr_ra[5:3], matr_ra[8:6]},
                      matr_choose, matr_we);

  reg [32:0] clk_in_div = 0;
  parameter BASE_CLK = 50_000_000;
  parameter OUT_FREQ = 25_000_000;
  wire [31:0] CLK_IN_DIV = (BASE_CLK) / OUT_FREQ - 1;

  reg clk_out = 0;
  always@(posedge clk) begin
    if (clk_in_div >= CLK_IN_DIV) begin
     clk_out <= !clk_out;
     clk_in_div <= 0;
    end
    else clk_in_div <= clk_in_div + 1;
  end

  // Shift clock
  reg [2:0] clk_out_div = 0;
  always@(posedge clk_out or negedge reset) begin
    if (!reset) clk_out_div <= 0;
    else clk_out_div <= clk_out_div + 1;
  end

  always@(negedge clk_out or negedge reset) begin
    if (!reset) shift_clk <= 0;
   else shift_clk <= !clk_out_div[2];
  end

  // Flip Flop clock
  reg [2:0] shift_clk_div = 7;
  always@(posedge shift_clk or negedge reset) begin
    if (!reset) shift_clk_div <= 7;
    else shift_clk_div <= shift_clk_div + 1;
  end

  always@(negedge shift_clk or negedge reset) begin
    if (!reset) latch_clk <= 0;
    else latch_clk <= !shift_clk_div[2];
  end

  // Layer Out Control
  always@(posedge latch_clk or negedge reset) begin
    if (!reset) layer_out <= 8'h40;
    else layer_out <= {layer_out[6:0], layer_out[7]};
  end

  // PWM clock and counter
  reg pwm_clk = 0;
  reg [5:0] pwm_div = 0;
  always@(negedge shift_clk or negedge reset) begin
    if (!reset) pwm_div <= 0;
    else pwm_div <= pwm_div + 1;
  end

  always@(posedge shift_clk or negedge reset) begin
    if (!reset) pwm_clk <= 0;
    else pwm_clk <= !pwm_div[5];
  end

  reg [7:0] pwm_count = 0;
  always@(posedge pwm_clk or negedge reset) begin
    if (!reset) pwm_count <= 0;
    else pwm_count <= pwm_count + 1;
  end

  // Shift Register Output
  always@(posedge clk_out or negedge reset) begin
    if (!reset) begin
      matr_ra <= 0;
      shift_out <= 0;
    end
    else begin
      matr_ra <= matr_ra + 1;
      shift_out[matr_ra[2:0]] <= matr_out > pwm_count;
    end
  end

  // Rasterizer State Machine
  parameter STATE_READY = 0;
  reg [5:0] state = STATE_READY;

  assign ready = clk & (state == STATE_READY);

  always@(posedge clk or negedge reset) begin
    if (!reset)
      state <= STATE_READY;
    else begin
      case (state)

      STATE_READY: begin
        case (func)
        `RASTER_POINTS: begin
          matr_wa <= pos_in;
          matr_in <= value;
        end
        endcase
      end

      endcase
    end
  end

endmodule
